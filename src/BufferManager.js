import BaseComponent from "./components/objects/BaseComponent";

/**
 * Управление буфером обмена
 */
export default class BufferManager {
    constructor(editor) {
        this._editor = editor;
        this._objects = editor.objects;
        this._countPastes = 0;
        this._moveSize = 10;

        document.addEventListener("copy", this.copyHandler);
        document.addEventListener("paste", this.pasteHandler);
        document.addEventListener("cut", this.cutHandler);
    }

    /**
     * Копирование объекта в буфер обмена
     */
    copyHandler = e => {
        const object = this._editor.getTransformatorObject();
        if (!(object && this._editor.isSelectObject())) {
            return false;
        }
        e.clipboardData.setData("text/plain", this.getObjectsData([object]));
        this._countPastes = 0;
        e.preventDefault();
    };

    /**
     * Вставка объекта с буфера обмена на холст
     */
    pasteHandler = e => {
        const objectsJson = e.clipboardData.getData("text/plain");
        this.openObjectsFromJson(objectsJson);
        e.preventDefault();
    };

    /**
     * Вырезание объекта с холста в буфер обмена
     */
    cutHandler = e => {
        const object = this._editor.getTransformatorObject();
        if (!(object && this._editor.isSelectObject())) {
            return false;
        }

        this._countPastes = -1;
        e.clipboardData.setData("text/plain", this.getObjectsData([object]));
        this._editor.deleteObjectAndChilds(object);
        e.preventDefault();
    };

    /**
     * Парсинг содержимого буфера обмена для вставки
     * @param {string} objectsJson
     */
    openObjectsFromJson(objectsJson) {
        let objects;
        try {
            objects = JSON.parse(objectsJson);
            this.increaseObjectsIds(objects);
            this.moveObjects(objects);
            this._editor.loadObjects(objects);
        } catch (e) {
            // Ignore incorrect buffer data
            return false;
        }
    }

    /**
     * Сдвигиваем клоны немного вправо и вниз, чтобы они не перекрывали исходные объекты
     */
    moveObjects(objects) {
        const isSelectObject = this._editor.isSelectObject();
        this._countPastes++;
        const lastMousePos = this._editor.lastMousePos;
        objects.forEach(object => {
            if (isSelectObject || lastMousePos === null) {
                object.x += this._countPastes * this._moveSize;
                object.y += this._countPastes * this._moveSize;
            } else {
                object.x = lastMousePos.x + this._countPastes * this._moveSize;
                object.y = lastMousePos.y + this._countPastes * this._moveSize;
            }
        });
    }

    /**
     * Назначение новых id для вставленных объектов
     * @param {*} objects
     */
    increaseObjectsIds(objects) {
        objects.forEach(object => {
            object.id = this._editor.getLastId();
        });
    }

    /**
     * Сериализация объектов в буфер обмена
     * @param {*} objects
     */
    getObjectsData(objects) {
        if (!(objects && this._editor.isSelectObject())) {
            return "";
        }
        return JSON.stringify(objects, (key, value) => {
            if (key === "childs") {
                return value;
            } else {
                return BaseComponent.prototype.jsonParseFilter(key, value);
            }
        });
    }

    // Выполнение нативных команд
    copy() {
        document.execCommand("copy");
    }

    cut() {
        document.execCommand("cut");
    }

    paste() {
        document.execCommand("paste");
    }

    /**
     * Клонирование объекта
     */
    clone() {
        const object = this._editor.getTransformatorObject();
        if (!(object && this._editor.isSelectObject())) {
            return false;
        }
        this._countPastes = 0;
        const cloneData = this.getObjectsData([object]);
        this.openObjectsFromJson(cloneData);
    }

    /**
     * Чистка объекта класса BufferManager для последующего удаления
     */
    destroy() {
        document.removeEventListener("copy", this.copyHandler);
        document.removeEventListener("paste", this.pasteHandler);
        document.removeEventListener("cut", this.cutHandler);
    }
}
