import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import PlanEditor from "./PlanEditor";
import registerServiceWorker from "./registerServiceWorker";

/**
 * Скрипт для демонстрации работы редактора плана, с фейковыми данными
 */
function loadDemoPlan() {
    return fetch('demoPlan.json').then(response => {
        return response.json();
    }).then(obj => {
        const imageData = obj.image;
        localStorage.setItem('image_a7dd091e7824d', JSON.stringify(imageData));
        const planData = obj.plan;
        return planData;
    });
}
if (window.chrome) {
    let ref;
    ReactDOM.render(
        <PlanEditor
            getSources={() => [
                { text: "Источник 1", value: 1 },
                { text: "Источник 2", value: 2 }
            ]}
            isEditor={!document.location.search.startsWith("?isView")}
            getPlans={() => [
                { text: "План1", value: 1 },
                { text: "План 22", value: 22 }
            ]}
            getMacroses={() => [
                { text: "Макрос1", value: 1 },
                { text: "Макрос 22", value: 22 }
            ]}
            getWebPages={() => [
                { text: "Страница 1", value: 1 },
                { text: "Страница 2", value: 3 }
            ]}
            getTours={() => [
                { text: "Тур 1", value: 1, count: 22 },
                { text: "Тур 2", value: 3, count: 3 }
            ]}
            getSalvos={() => [
                { text: "Salvo 1", value: 1 },
                { text: "Salvo 2", value: 3 }
            ]}
            onAction={(actionName, actionValue) =>
                console.warn(actionName, actionValue)
            }
            onDragEnd={() => 0}
            onChange={ () =>  localStorage.setItem('plan_1', JSON.stringify(ref.savePlan())) }
            ref={r => {
                ref = r;

                if (window.location.hash === '#demo') {
                    loadDemoPlan().then(plan => r.openPlan(plan));
                    window.location.hash = '';
                    return;
                }
                const data = localStorage.getItem('plan_1');
                let plan;
                if (data) {
                    try {
                        plan = JSON.parse(data);
                    } catch (e) {
                        console.error("Plan data is fault. Load new plan");
                    }
                }

                if (plan) {
                    r.openPlan(plan);
                }
            }}
        />,
        document.getElementById("root")
    );
    registerServiceWorker();
} else {
    alert("Данное приложение не работает на браузерах, отличных от Google chrome.");
}
window.ReactJS = {
    React,
    PlanEditor,
    ReactDOM
};
