export default class ObjectsManager {
    constructor(plan) {
        this._objects = [];
        this._plan = plan;
    }

    get objects() {
        return this._objects;
    }

    get count() {
        return this._objects.length;
    }

    getObjectIndex(object) {
        return this._objects.indexOf(object);
    }

    getLastId() {
        return this.objects.length;
    }

    getObjectByIndex(index) {
        return this._objects[index];
    }

    add(object) {
        this._objects.push(object);
    }

    getLastObject() {
        return this._objects[this.count - 1];
    }

    /**
     * Переключение блокировки объекта
     * @param {Number} ind
     */
    toggleObjectLock = ind => {
        const objects = this._objects;
        const object = objects[ind];

        if (object) {
            object.locked = !object.locked;
        }
        this._plan.updateIterationObject(object);
        this._plan.memento.takeSnapshot(
            object,
            this._plan.oldSelectedComponentIndex,
            this._plan.state.selectedComponentIndex
        );
    };

    movingObject(object, pos) {
        object.x += pos % 2 === 0 ? 3 - pos : 0;
        object.y += pos % 2 === 1 ? 2 - pos : 0;

        this._plan.updateIterationObject(object);
        this._plan.memento.takeSnapshotDebounce(
            object,
            this._plan.oldSelectedComponentIndex,
            this._plan.state.selectedComponentIndex
        );
    }

    moveZObjectTo(object, from, to, isUndoRedo = false) {
        if (to < 0 || to > this.count) {
            return;
        }

        const objects = this._objects;
        objects.splice(to, 0, objects.splice(from, 1)[0]);

        const objectsIterations = this._plan.state.objectsIterations;
        objectsIterations.splice(to, 0, objectsIterations.splice(from, 1)[0]);
        this._plan.setState({ objectsIterations });

        this._plan.updateIterationObject(object);
        if (from === this._plan.state.selectedComponentIndex) {
            this._plan.changeSelectIndexComponent(to);
        }
        if (!isUndoRedo) {
            this._plan.memento.moveZObject(
                object.id,
                from,
                to,
                this._plan.oldSelectedComponentIndex,
                this._plan.state.selectedComponentIndex
            );
        }
    }

    /**
     * Переносим объект по слоям
     * @param {Object} object removing object
     * @param {Number} step add to z-axis
     */
    moveZObject(object, step) {
        const from = this.getObjectIndex(object);
        if (from === -1) {
            return;
        }
        const to = from + step;

        this.moveZObjectTo(object, from, to);
    }

    /**
     * Переносим объект по слоям - вперед
     * @param {Object} obj целевой объект
     */
    moveZObjectUp = object => {
        this.moveZObject(object, 1);
    };

    /**
     * Переносим объект по слоям - назад
     * @param {Object} obj целевой объект
     */
    moveZObjectDown = object => {
        this.moveZObject(object, -1);
    };

    /**
     * Является ли объект блокирующим скролл по клику на себя
     */
    isObjectScrollBroke(object) {
        if (object && object.component) {
            return object.component.meta.isPreventScroll;
        }
        return false;
    }

    /**
     * Получение объекта по унимальному идентификатору
     */
    getObjectById(id, objects = this._objects) {
        if (id === -1) {
            return this._plan.state.canvas;
        }
        for (const obj of objects) {
            if (+obj.id === +id) {
                return obj;
            }
        }
        return null;
    }
}
