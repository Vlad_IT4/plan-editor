import React, { Component } from "react";
import PropTypes from "prop-types";
/**
 * Предварительный конструктор компонента "фигура".
 * Вызывается при добавлении компонента на холст.
 * Служит для построения точек фигуры.
 */
export default class PathEditor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            points: [],
            lastPoint: null,
            mouse: null
        };
    }
    /**
     * Добавление точки для пути
     * @param {Event} ev
     */
    addPoint(ev) {
        const { x, y } = this.state.mouse;
        const points = this.state.points;
        // Добавляем точку, куда пользователь кликнул мышью
        points.push({ x, y });

        if (points.length > 1 && x === points[0].x && y === points[0].y) {
            // Производим смещение все точек внулю, чтобы не было точек с отрицательными координатами
            let ranges = {
                minX: points[0].x,
                minY: points[0].y,
                maxX: points[0].x,
                maxY: points[0].y
            };
            points.forEach(point => {
                ranges.minX = Math.min(point.x, ranges.minX);
                ranges.minY = Math.min(point.y, ranges.minY);
                ranges.maxX = Math.max(point.x, ranges.maxX);
                ranges.maxY = Math.max(point.y, ranges.maxY);
            });

            let offsetZeroPoints = points.map(point => ({
                x: point.x - ranges.minX,
                y: point.y - ranges.minY
            }));

            this.props.object.width = ranges.maxX - ranges.minX;
            this.props.object.height = ranges.maxY - ranges.minY;

            this.props.object.x = ranges.minX;
            this.props.object.y = ranges.minY;
            this.props.object.isEditing = false;
            this.props.object.points = offsetZeroPoints;

            this.props.onEditEnd(this.props.object);
        } else {
            this.setState({
                points,
                lastPoint: { x, y }
            });
        }
    }

    onMouseMove(ev) {
        let { offsetX: mx, offsetY: my } = ev.nativeEvent;

        mx = (mx - this.props.scroll.x) / this.props.zoom;
        my = (my - this.props.scroll.y) / this.props.zoom;

        if (this.state.points.length) {
            const { x: px, y: py } = this.state.points[0];
            // Делаем "примагничивание" мыши к первой точки, чтобы можно было легко закрыть фигуру
            if (mx > px - 10 && mx < px + 10 && my > py - 10 && my < py + 10) {
                mx = px;
                my = py;
            }
        }

        this.setState({ mouse: { x: mx, y: my } });
    }
    render() {
        const points = this.state.points.map((point, ind) => {
            const modifer = ind === 0 ? "M" : "L"; // Первая точка будет оператором Moveto
            return `${modifer} ${point.x} ${point.y}`;
        });

        return (
            <g>
                <rect
                    fill="#33335555"
                    style={{ fillOpacity: 0.33 }}
                    x={-10000}
                    y={-10000}
                    width={this.props.canvWidth + 20000}
                    height={this.props.canvHeight + 20000}
                    onClick={this.addPoint.bind(this)}
                    onMouseMove={this.onMouseMove.bind(this)}
                />

                <g style={{ pointerEvents: "none" }}>
                    {this.state.lastPoint && this.state.mouse && (
                        <line
                            x1={this.state.lastPoint.x}
                            y1={this.state.lastPoint.y}
                            x2={this.state.mouse.x}
                            y2={this.state.mouse.y}
                            stroke="#AA0000"
                            strokeWidth="1"
                        />
                    )}
                    <path
                        d={points.join(" ")}
                        fill="transparent"
                        stroke="black"
                    />
                    {this.state.points.map((point, ind) => (
                        <circle
                            cx={point.x}
                            cy={point.y}
                            r="2"
                            fill={ind === 0 ? "lime" : "red"}
                            key={ind}
                        />
                    ))}
                </g>
            </g>
        );
    }
}

PathEditor.propTypes = {
    object: PropTypes.object,
    scroll: PropTypes.object,
    zoom: PropTypes.number,
    canvWidth: PropTypes.number,
    canvHeight: PropTypes.number,
    onEditEnd: PropTypes.func
};
