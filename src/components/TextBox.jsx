import React, { PureComponent } from "react";
import PropTypes from "prop-types";

/**
 * Блок с текстом
 */
export default class TextBox extends PureComponent {
    static defaultProps = {
        fontSize: 25
    };

    render() {
        return (
            <foreignObject
                width="350"
                height="350"
                style={{ textAlign: "center" }}
            >
                <div
                    xmlns="http://www.w3.org/1999/xhtml"
                    className={"object__title " + this.props.className}
                    style={{ fontSize: this.props.fontSize }}
                >
                    {this.props.children}
                </div>
            </foreignObject>
        );
    }
}

TextBox.propTypes = {
    className: PropTypes.string,
    fontSize: PropTypes.string,
    children: PropTypes.string
};
