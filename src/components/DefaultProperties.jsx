import Fields from "./fields";

export default {
    component: {
        title: "Тип",
        type: Fields.Field,
        readonly: true,
        forChildren: true
    },
    name: {
        type: Fields.Field,
        forChildren: true
    },
    angle: {
        title: "Поворот",
        type: Fields.RangeField,
        maxValue: 360,
        step: 0.5,
        defaultValue: 0
    },
    scale: {
        title: "Масштаб",
        type: Fields.RangeField,
        maxValue: 15,
        minValue: 0.2,
        step: 0.01,
        defaultValue: 0.5
    },
    x: {
        title: "X",
        type: Fields.NumberField,
        forChildren: true
    },
    y: {
        title: "Y",
        type: Fields.NumberField,
        forChildren: true
    },
    fill: {
        title: "Заливка",
        type: Fields.ColorField,
        defaultValue: "darkgray"
    },
    stroke: {
        title: "Цвет обводки",
        type: Fields.ColorField,
        defaultValue: "#000000"
    },
    strokeWidth: {
        title: "Ширина обводки",
        type: Fields.RangeField,
        maxValue: 50,
        minValue: 1,
        defaultValue: 1
    },
    opacity: {
        title: "Непрозрачность",
        type: Fields.RangeField,
        maxValue: 100,
        minValue: 0,
        defaultValue: 100
    },
    locked: {
        type: Fields.CheckboxField,
        defaultValue: false,
        forChildren: true
    },
    showText: {
        title: "Показывать описание",
        type: Fields.CheckboxField,
        defaultValue: true
    },
    source: {
        title: "Открыть план",
        type: Fields.ListField,
        parcel: "plans",
        hideElementIfNone: false
    },
    fontSize: {
        title: "Размер шрифта описания",
        type: Fields.RangeField,
        defaultValue: 14,
        minValue: 4,
        maxValue: 50
    }
};
