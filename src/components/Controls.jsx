import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { SvgManager } from "../Helpers.js";

// Рисуем кнопку в списке команд
function ControlItem(props) {
    return (
        <span
            className={
                "controls__button" +
                (props.disabled ? " controls__button_disabled" : "")
            }
            onClick={props.disabled ? undefined : props.onClick}
            title={props.title}
        >
            <svg width="16px" height="16px" viewBox="0 0 64 64">
                <use xlinkHref={SvgManager.get(props.icon)} />
            </svg>
        </span>
    );
}

/**
 * Список команд
 */
export default class Controls extends PureComponent {
    render() {
        return (
            <div className="controls__list">
                <ControlItem
                    disabled={!this.props.isSelectObject}
                    title="Вырезать"
                    onClick={this.props.onCutClick}
                    icon="cut"
                />
                <ControlItem
                    disabled={!this.props.isSelectObject}
                    title="Копировать"
                    onClick={this.props.onCopyClick}
                    icon="copy"
                />
                <ControlItem
                    disabled={false}
                    title="Вставить"
                    onClick={this.props.onPasteClick}
                    icon="paste"
                />
                <ControlItem
                    disabled={!this.props.isSelectObject}
                    title="Дублировать"
                    onClick={this.props.onCloneClick}
                    icon="clone"
                />
                <ControlItem
                    disabled={!this.props.canUndo}
                    title="Отменить"
                    onClick={this.props.onUndoClick}
                    icon="undo"
                />
                <ControlItem
                    disabled={!this.props.canRedo}
                    title="Вернуть"
                    onClick={this.props.onRedoClick}
                    icon="redo"
                />
            </div>
        );
    }
}

Controls.propTypes = {
    isSelectObject: PropTypes.bool,
    canUndo: PropTypes.bool,
    onUndoClick: PropTypes.func,
    onRedoClick: PropTypes.func,
    onCutClick: PropTypes.func,
    onCopyClick: PropTypes.func,
    onPasteClick: PropTypes.func
};
