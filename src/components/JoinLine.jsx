import React from 'react';
import PropTypes from 'prop-types';

/**
 * Линия, соеденяющая родительский компонент, с дочерними
 */
export default class JoinLine extends React.Component {
    render() {
        const parent = this.props.parent;
        const child = this.props.child;

        const parentX = parent.x + this.props.parentCenter.x * parent.scale;
        const parentY = parent.y + this.props.parentCenter.y * parent.scale;

        let childX;
        let childY;

        if ('scale' in child) {
            childX = child.x * child.scale;
            childY = child.y * child.scale;
        } else if ('width' in child && 'height' in child ){
            childX = child.x + child.width / 2;
            childY = child.y + child.height / 2;
        } else if ('x1' in child) {
            childX = child.x + Math.abs(child.x2 - child.x1) / 2;
            childY = child.y + Math.abs(child.y2 - child.y1) / 2;
        }

        return (
            <path d={ `M ${parentX},${parentY} ${childX},${childY}` }
                style={{ pointerEvents: 'none', stroke:'#448866', strokeWidth:'2px', strokeOpacity:0.4, strokeDasharray: '4px'}} />
        );
    }
}

JoinLine.propTypes = {
    parent: PropTypes.object,
    child: PropTypes.object,
    parentCenter: PropTypes.object,
};