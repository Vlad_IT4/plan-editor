import React, { Component } from "react";
import PropTypes from "prop-types";

/**
 * Редактор свойств объекта
 */
export default class ProportiesEditor extends Component {
    // Список свойств, которые недоступны для редактирования через редактор
    static ignoreChildProps = [
        "action",
        "fill",
        "stroke",
        "opacity",
        "strokeWidth",
        "showText",
        "source",
        "fontSize"
    ];

    shouldComponentUpdate(n) {
        const p = this.props;

        for (let key in n) {
            const newVal = n[key];
            const oldVal = p[key];

            if (newVal !== oldVal) {
                return true;
            }
        }
        return false;
    }

    // Следим за изменениями
    handleChange = (key, value) => {
        const oldValue = this.props.object[key];

        if (oldValue === value) {
            return;
        }

        this.props.onChange(this.props.objectIndex, key, value);
    }

    handleChangeEnd = (key, value) => {
        this.props.onChangeEnd(this.props.objectIndex, key, value);
    };

    // Кнопки перемещения объекта по слоям
    renderZButtons() {
        return (
            <div className="properties__item">
                <div className="properties__label">Расположение</div>
                <div className="properties__value">
                    <button
                        disabled={this.props.isFirstObject}
                        onClick={() =>
                            this.props.moveZObjectDown(this.props.object)
                        }
                        className="button"
                    >Отправить назад</button>
                    <button
                        disabled={this.props.isLastObject}
                        onClick={() =>
                            this.props.moveZObjectUp(this.props.object)
                        }
                        className="button"
                    >Отправить вперед</button>
                </div>
            </div>
        );
    }

    render() {
        if (this.props.object) {
            const props = this.props.object.component.meta.properties;
            const isChildren = typeof this.props.object.parent === "number";

            return (
                <div className="properties__block panel-properties">
                    <h2 className="properties__title">Свойства</h2>
                    {props["zIndex"] !== null && this.renderZButtons()}
                    {Object.keys(props).map((key, ind) => {
                        let settings = props[key];

                        if (!(settings && settings.title)) {
                            return null;
                        }

                        if (isChildren && !settings.forChild) {
                            return null;
                        }

                        let Comp = settings.type;
                        let parcel;

                        if (settings.parcel) {
                            let parcelName = settings.parcel;
                            if (
                                this.props.parcels &&
                                parcelName in this.props.parcels
                            ) {
                                const parcelPost = this.props.parcels[
                                    parcelName
                                ];
                                if (typeof parcelPost === "function") {
                                    let args = {};
                                    if (settings.arguments) {
                                        args = settings.arguments;
                                    }
                                    parcel = parcelPost(args);
                                } else if (
                                    parcelPost &&
                                    typeof parcelPost === "object" &&
                                    !Array.isArray(parcelPost)
                                ) {
                                    parcel = {};
                                    for (var name in parcelPost) {
                                        const func = parcelPost[name].func;
                                        if (func) {
                                            parcel[name] = {
                                                text: parcelPost[name].text,
                                                list: func()
                                            };
                                        }
                                    }
                                } else {
                                    parcel = parcelPost;
                                }
                            }
                        }
                        let value = this.props.object[key];
                        if (key === "component") {
                            value = this.props.object[key].meta.name;
                        }
                        return (
                            <Comp
                                selectObject={this.props.selectObject}
                                createComponent={this.props.createComponent}
                                object={this.props.object}
                                name={key}
                                title={settings.title}
                                value={value}
                                key={ind}
                                onChange={this.handleChange}
                                onChangeEnd={this.handleChangeEnd}
                                parcel={parcel}
                                settings={settings}
                            />
                        );
                    })}
                </div>
            );
        } else {
            return null;
        }
    }
}

ProportiesEditor.propTypes = {
    object: PropTypes.object,
    createComponent: PropTypes.func,
    moveZObjectDown: PropTypes.func,
    moveZObjectUp: PropTypes.func,
    parcels: PropTypes.object,
    isFirstObject: PropTypes.bool,
    isLastObject: PropTypes.bool,
    onChange: PropTypes.func,
    onChangeEnd: PropTypes.func
};
