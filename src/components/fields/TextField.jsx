import Field from "./Field";

/**
 * Свойтсов объекта: текстовое поле ввода
 */
export default class TextField extends Field {
    getValue() {
        let defaultValue = "";

        if (this.props.settings.defaultValue !== undefined) {
            defaultValue = this.props.settings.defaultValue;
        }

        return this.props.value === undefined ? defaultValue : this.props.value;
    }

    handleChange = event => {
        this.props.onChange(this.props.name, event.target.value);
    };

    handleChangeEnd = event => {
        this.props.onChangeEnd(this.props.name, event.target.value);
    };
}
