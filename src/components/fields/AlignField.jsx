import React from "react";
import Field from "./Field";

/**
 * Ячейка таблицы выравнивания фона
 */
function AlignItem(props) {
    return (
        <div className="property-align__item">
            <input
                type="radio"
                name={props.id}
                id={`${props.id}_${props.postfix}`}
                className="property-align__radio"
                value={props.value}
                checked={props.checked}
                onChange={props.onChange}
            />
            <label
                htmlFor={`${props.id}_${props.postfix}`}
                className="property-align__label"
            />
        </div>
    );
}

/**
 * Свойтсов объекта: выравнивание фона
 */
export default class AlignField extends Field {
    constructor(props) {
        super(props);
        this.id = Math.random().toString(35);
    }

    onChange = e => {
        const args = [this.props.name, e.target.value];
        this.props.onChange(...args);
        this.props.onChangeEnd(...args);
    };

    render() {
        const value = this.getValue();

        return (
            <div className="properties__item">
                <div className="properties__label">{this.getName()}</div>
                <div className="properties__value">
                    <div className="property-align">
                        <div className="property-align__row">
                            <AlignItem
                                id={this.id}
                                postfix="tl"
                                value="xMinYMin"
                                checked={value === "xMinYMin"}
                                onChange={this.onChange}
                            />
                            <AlignItem
                                id={this.id}
                                postfix="tc"
                                value="xMidYMin"
                                checked={value === "xMidYMin"}
                                onChange={this.onChange}
                            />
                            <AlignItem
                                id={this.id}
                                postfix="tr"
                                value="xMaxYMin"
                                checked={value === "xMaxYMin"}
                                onChange={this.onChange}
                            />
                        </div>
                        <div className="property-align__row">
                            <AlignItem
                                id={this.id}
                                postfix="ml"
                                value="xMinYMid"
                                checked={value === "xMinYMid"}
                                onChange={this.onChange}
                            />
                            <AlignItem
                                id={this.id}
                                postfix="mc"
                                value="xMidYMid"
                                checked={value === "xMidYMid"}
                                onChange={this.onChange}
                            />
                            <AlignItem
                                id={this.id}
                                postfix="mr"
                                value="xMaxYMid"
                                checked={value === "xMaxYMid"}
                                onChange={this.onChange}
                            />
                        </div>
                        <div className="property-align__row">
                            <AlignItem
                                id={this.id}
                                postfix="bl"
                                value="xMinYMax"
                                checked={value === "xMinYMax"}
                                onChange={this.onChange}
                            />
                            <AlignItem
                                id={this.id}
                                postfix="bc"
                                value="xMidYMax"
                                checked={value === "xMidYMax"}
                                onChange={this.onChange}
                            />
                            <AlignItem
                                id={this.id}
                                postfix="br"
                                value="xMaxYMax"
                                checked={value === "xMaxYMax"}
                                onChange={this.onChange}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
