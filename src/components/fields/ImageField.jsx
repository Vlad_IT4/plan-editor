import Field from "./Field";
import React from "react";
import ImageManager from "../ImageManager";
import { SvgManager } from "../../Helpers.js";

/**
 * Свойтсов объекта: выбор изображения
 */
export default class ImageField extends Field {
    constructor(props) {
        super(props);
        this.state = {
            isSelectingImage: false
        };
    }

    /**
     * Перерисовываем компонент, также когда поменяется состояние показа окна загрузки картинки
     */
    shouldComponentUpdate(n, state) {
        if (super.shouldComponentUpdate(n, state)) {
            return true;
        }

        return state.isSelectingImage !== this.state.isSelectingImage;
    }

    /**
     * Открыть окно выбора картинки
     */
    openImageSelector() {
        this.setState({ isSelectingImage: true });
    }

    /**
     * Закрыть окно выбора картинки
     */
    closeImageSelector() {
        this.setState({ isSelectingImage: false });
    }

    onSelectImage(e) {
        const callBackArgs = [this.props.name, e];
        this.props.onChange(...callBackArgs);
        this.closeImageSelector();
    }

    render() {
        return (
            <div className="properties__item">
                <div className="properties__label">{this.getName()}</div>
                <div className="properties__value">
                    {this.state.isSelectingImage ? (
                        <ImageManager
                            onSelect={e => this.onSelectImage(e)}
                            onCancel={this.closeImageSelector.bind(this)}
                        />
                    ) : null}
                    <button onClick={this.openImageSelector.bind(this)}>
                        <svg
                            width="16px"
                            height="16px"
                            viewBox="0 0 469.336 469.336"
                        >
                            <use xlinkHref={SvgManager.get("edit")} />
                        </svg>
                    </button>
                </div>
            </div>
        );
    }
}
