import React from "react";
import Field from "./Field";

import { SvgManager } from "../../Helpers.js";

/**
 * Свойтсов объекта: выбор иконки объекта из набора
 */
export default class IconSelectField extends Field {
    constructor(props) {
        super(props);
        this.state = {
            isOpenIconSelector: false
        };
        this.iconSelect = React.createRef();
    }

    /**
     * Перерисовываем компонент, так же, когда поменяется состояние "открытости" окна выбора иконки
     */
    shouldComponentUpdate(n, state) {
        if (super.shouldComponentUpdate(n, state)) {
            return true;
        }

        return state.isOpenIconSelector !== this.state.isOpenIconSelector;
    }

    componentDidMount() {
        document.addEventListener("mousedown", this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener("mousedown", this.handleClickOutside);
    }

    /**
     * Закрываем окно выбора иконки, если пользователь кликнул за пределы окна
     */
    handleClickOutside = event => {
        if (
            this.iconSelect.current &&
            !this.iconSelect.current.contains(event.target)
        ) {
            this.hideIconSelect();
        }
    };

    /**
     * Показываем окно выбора иконки
     */
    showIconSelect = () => {
        this.setState({ isOpenIconSelector: true });
    }

    /**
     * Прячем окно выбора иконки
     */
    hideIconSelect = () => {
        this.setState({ isOpenIconSelector: false });
    }

    onSelectIcon = e => {
        const callBackArgs = [this.props.name, e.target.value];
        this.props.onChange(...callBackArgs);
        this.props.onChangeEnd(...callBackArgs);
        this.hideIconSelect();
    };

    render() {
        return (
            <div className="properties__item" style={{ position: "relative" }}>
                <div className="properties__label">{this.getName()}</div>
                <div className="properties__value">
                    <svg width="64px" height="64px" fill="darkgray">
                        <use xlinkHref={SvgManager.get(this.getValue())} />
                    </svg>
                    <button onClick={this.showIconSelect}>
                        <svg
                            width="16px"
                            height="16px"
                            viewBox="0 0 469.336 469.336"
                        >
                            <use xlinkHref={SvgManager.get("edit")} />
                        </svg>
                    </button>
                    <div
                        className={
                            "icon-select " +
                            (this.state.isOpenIconSelector
                                ? "icon-select_visible"
                                : "")
                        }
                        ref={this.iconSelect}
                    >
                        {this.props.settings.icons.map(iconId => (
                            <label
                                className={
                                    "icon-select__item " +
                                    (this.props.value === iconId
                                        ? "icon-select__item_active"
                                        : "")
                                }
                                key={iconId}
                            >
                                <input
                                    type="radio"
                                    name={`icon_${
                                        this._reactInternalFiber.key
                                    }`}
                                    value={iconId}
                                    onClick={this.onSelectIcon}
                                    className="icon-select__radio"
                                />
                                <svg width="64px" height="64px" fill="darkgray">
                                    <use xlinkHref={SvgManager.get(iconId)} />
                                </svg>
                                <span className="icon-select__title">
                                    {iconId}
                                </span>
                            </label>
                        ))}
                    </div>
                </div>
            </div>
        );
    }
}
