import Field from "./Field";
import TextField from "./TextField";
import ImageField from "./ImageField";
import RangeField from "./RangeField";
import ColorField from "./ColorField";
import IconSelectField from "./IconSelectField";
import CheckboxField from "./CheckboxField";
import NumberField from "./NumberField";
import ListField from "./ListField";
import ActionField from "./ActionField";
import SectorListField from "./SectorListField";
import AlignField from "./AlignField";

// Доступные свойства объектов
export default {
    Field,
    TextField,
    ImageField,
    AlignField,
    RangeField,
    ColorField,
    IconSelectField,
    CheckboxField,
    NumberField,
    ListField,
    ActionField,
    SectorListField
};
