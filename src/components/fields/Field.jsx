import React, { Component } from "react";
import PropTypes from "prop-types";

/**
 * Базовый компонент всех свойств объектов
 */
export default class Field extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    /**
     * Перерисовываем свойство, только в случае изменения ссылки на модель изменяемого объекта, или полей name, value
     */
    shouldComponentUpdate(n, state) {
        const p = this.props;

        return !(
            n.name === p.name &&
            n.value === p.value &&
            state.value === this.state.value &&
            n.object === p.object
        );
    }

    /**
     * Получить заголовок свойства
     */
    getName() {
        return this.props.title;
    }

    /**
     * Получить значение свойства. Если значения нет, то возвращаем значение по умолчанию данного свойства для данного изменяемого объекта
     */
    getValue() {
        if (typeof this.props.value === "number") {
            return +Number(this.props.value).toFixed(2);
        }

        let defaultValue = "";

        if (this.props.settings.defaultValue !== undefined) {
            defaultValue = this.props.settings.defaultValue;
        }

        return this.props.value === undefined ? defaultValue : this.props.value;
    }

    /**
     * Следим за каждым изменением свойства, для перерисовки компонента на холсте
     */
    handleChange = event => {
        this.props.onChange(this.props.name, +event.target.value);
    };

    /**
     * Следим за конечным изменением свойства, для его сохранения или создания снэпшота для undo/redo
     */
    handleChangeEnd = event => {
        this.props.onChangeEnd(this.props.name, +event.target.value);
    };

    /**
     * Базовый рендеринг для компонентов, которые не будут переопределять этот метод, или будут его расширять (подставляя свое значение type)
     * @param {string} type значение атрибута type у html элемента input.
     */
    render(type = "text") {
        return (
            <div className="properties__item">
                <div className="properties__label">{this.getName()}</div>
                <div className="properties__value">
                    <input
                        type={type}
                        value={this.getValue()}
                        onBlur={this.handleChangeEnd}
                        onChange={this.handleChange}
                        disabled={this.props.settings.readonly}
                        className="input"
                    />
                </div>
            </div>
        );
    }
}

Field.propTypes = {
    onChange: PropTypes.func, // Событие каждого изменения свойства
    onChangeEnd: PropTypes.func, // Событие конечного изменения свойства
    name: PropTypes.string, // Заголовок свойства
    settings: PropTypes.object // Параметры свойства
};
