import Field from "./Field";

/**
 * Свойтсов объекта: поле ввода числа
 */
export default class NumberField extends Field {
    render() {
        return super.render("number");
    }
}
