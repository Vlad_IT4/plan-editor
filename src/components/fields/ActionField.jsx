import React from "react";
import Field from "./Field";
import PropTypes from "prop-types";

/**
 * Свойство объектов: выбор действия по клику
 */
export default class ActionField extends Field {
    constructor(props) {
        super(props);
        this.state = {
            name: null,
            value: null
        };
    }

    /**
     * Событие выбора действия, вызывает базовые события изменения значения свойства
     * @param {string} name действие
     */
    onChangeAction(name) {
        this.setState({
            name,
            value: null
        });
        const callBackArgs = [
            this.props.name,
            {
                name: name,
                object: null
            }
        ];
        this.props.onChange(...callBackArgs);
        this.props.onChangeEnd(...callBackArgs);
    }

    /**
     * Событие выбора целевого объекта для выбранного действия
     * @param {String} value объект
     */
    onChangeActionObject(value) {
        this.setState({
            value: value
        });

        this.props.onChange(this.props.name, {
            name: this.state.name,
            object: value
        });
    }

    /**
     * Вывод списка действий
     */
    getActionList() {
        return Object.keys(this.props.parcel).map(key => (
            <option value={key} key={key}>
                {this.props.parcel[key].text}
            </option>
        ));
    }

    render() {
        const actionsList = this.getActionList();

        return (
            <div className="properties__item">
                <div className="properties__label">{this.getName()}</div>
                <div className="properties__value properties__value_rows">
                    <select
                        className="poperties__select_small"
                        onChange={e => this.onChangeAction(e.target.value)}
                        defaultValue={this.props.value.name}
                    >
                        <option>Без действия</option>
                        {actionsList}
                    </select>
                    {this.props.parcel[this.props.value.name] && (
                        <select
                            className="poperties__select_small"
                            onChange={e =>
                                this.onChangeActionObject(e.target.value)
                            }
                            defaultValue={this.props.value.object}
                        >
                            <option disabled selected>
                                выберите
                            </option>
                            {this.props.parcel[this.props.value.name].list.map(
                                (el, ind) => (
                                    <option value={el.value} key={ind}>
                                        {el.text}
                                    </option>
                                )
                            )}
                        </select>
                    )}
                </div>
            </div>
        );
    }
}

ActionField.propTypes = {
    onChange: PropTypes.func,
    onChangeEnd: PropTypes.func,
    name: PropTypes.string,
    parcel: PropTypes.array,
    value: PropTypes.object
};
