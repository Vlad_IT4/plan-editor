import Field from "./Field";
import React from "react";

/**
 * Свойтсов объекта: чекбокс
 */
export default class CheckboxField extends Field {
    render() {
        return (
            <div className="properties__item">
                <div className="properties__label">{this.getName()}</div>
                <div className="properties__value">
                    <input
                        type="checkbox"
                        checked={this.getValue()}
                        className="properties__checked"
                        onChange={e =>
                            this.props.onChange(
                                this.props.name,
                                e.target.checked
                            )
                        }
                    />
                </div>
            </div>
        );
    }
}
