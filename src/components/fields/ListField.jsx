import Field from "./Field";
import React from "react";

/**
 * Свойтсов объекта: выпадающий список
 */
export default class ListField extends Field {
    onChange = ev => {
        const args = [this.props.name, ev.target.value];
        this.props.onChange(...args);
        this.props.onChangeEnd(...args);
    };

    render() {
        const list = this.props.parcel.map((el, ind) => (
            <option value={el.value} key={ind}>
                {el.text}
            </option>
        ));

        return (
            <div className="properties__item">
                <div className="properties__label">{this.getName()}</div>
                <div className="properties__value">
                    <select
                        onChange={this.onChange}
                        value={this.getValue()}
                        className="properties__select"
                    >
                        {!this.props.settings.hideEmpty && <option value="" />}
                        {list}
                    </select>
                </div>
            </div>
        );
    }
}
