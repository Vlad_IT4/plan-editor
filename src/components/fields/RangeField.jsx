import Field from "./Field";
import React from "react";

/**
 * Свойтсов объекта: ползунок
 */
export default class RangeField extends Field {
    render() {
        return (
            <div className="properties__item">
                <div className="properties__label">{this.getName()}</div>
                <div className="properties__value">
                    <input
                        type="number"
                        min={this.props.settings.minValue || 0}
                        max={this.props.settings.maxValue || 1000}
                        value={this.getValue()}
                        className="properties__range-text input"
                        onBlur={this.handleChangeEnd}
                        onMouseUp={this.handleChangeEnd}
                        onChange={this.handleChange}
                    />
                    <input
                        type="range"
                        value={this.getValue()}
                        min={this.props.settings.minValue || 0}
                        max={this.props.settings.maxValue || 1000}
                        step={this.props.settings.step}
                        className="properties__range-line"
                        onBlur={this.handleChangeEnd}
                        onKeyUp={this.handleChangeEnd}
                        onMouseUp={this.handleChangeEnd}
                        onChange={this.handleChange}
                    />
                </div>
            </div>
        );
    }
}
