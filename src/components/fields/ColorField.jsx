import React from "react";
import Field from "./Field";

/**
 * Свойтсов объекта: выбор цвета
 */
export default class ColorField extends Field {
    onChange = e => {
        const args = [this.props.name, e.target.value];
        this.props.onChange(...args);
        this.props.onChangeEnd(...args);
    };
    render() {
        return (
            <div className="properties__item">
                <div className="properties__label">{this.getName()}</div>
                <div className="properties__value">
                    <input
                        type="text"
                        value={this.getValue()}
                        className="promerties__color-text input"
                        onChange={this.onChange}
                    />
                    <input
                        type="color"
                        value={this.getValue()}
                        className="properties__color-button input"
                        onChange={this.onChange}
                    />
                </div>
            </div>
        );
    }
}
