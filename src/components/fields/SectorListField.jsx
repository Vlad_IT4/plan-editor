import Field from "./Field";
import React from "react";
import Objects from "../objects/objects.js";
import { SvgManager } from "../../Helpers.js";

/**
 * Список дочерних компонентов поля зрения объекта
 */
class SectorComponentList extends React.PureComponent {
    render() {
        return Object.keys(Objects).map((name, ind) => {
            const comp = Objects[name];

            if (!comp.meta.isShape) {
                return null;
            }

            return (
                <div className="child" key={ind}>
                    <div
                        className="child__image"
                        style={{ cursor: "pointer" }}
                        title={comp.meta.title}
                        draggable="true"
                        onDragStart={ev => this.props.onDragStart(ev, name)}
                        onClick={() => this.props.onClick(comp)}
                    >
                        <svg
                            width="16px"
                            height="16px"
                            viewBox="0 0 64 64"
                            version="1.1"
                            fill="darkgray"
                        >
                            <use xlinkHref={SvgManager.get(comp.meta.icon)} />
                        </svg>
                    </div>
                </div>
            );
        });
    }
}

/**
 * Свойтсов объекта: управление дочерними объектами поля зрения объекта
 */
export default class SectorListField extends Field {
    onChangeValue = event => {
        const callBackArgs = [this.props.name, +event.target.value];
        this.props.onChange(...callBackArgs);
        this.props.onChangeEnd(...callBackArgs);
    };

    /**
     * Добавление нового объекта
     */
    createComponent = comp => {
        this.props.createComponent(
            comp,
            this.props.object.x +
                this.props.object.scale * (Math.random() - 0.5) * 64,
            this.props.object.y +
                this.props.object.scale * (Math.random() - 0.5) * 64,
            this.props.object
        );
    };

    /**
     * Начало пермещения объекта на холст для его добавления к текущему объекту
     */
    onDragStart = (ev, compName) => {
        const event = ev.nativeEvent;
        event.dataTransfer.dropEffect = "move";
        event.dataTransfer.setData("component", compName);
        event.dataTransfer.setData("parent", this.props.object.id);
    };

    render() {
        return (
            <div
                className="properties__item"
                style={{ alignItems: "flex-start" }}
            >
                <div className="properties__label">{this.getName()}</div>
                <div className="properties__list">
                    <div className="sector-component-list">
                        <SectorComponentList
                            onClick={this.createComponent}
                            onDragStart={this.onDragStart}
                        />
                    </div>
                    {this.props.object.childs && (
                        <div className="sector-list">
                            {this.props.object.childs.map(obj => {
                                return (
                                    <div
                                        className="sector-list__item"
                                        onClick={e =>
                                            this.props.selectObject(obj)
                                        }
                                    >
                                        <div className="sector-list__icon">
                                            <svg
                                                width="16px"
                                                height="16px"
                                                viewBox="0 0 64 64"
                                                fill="darkgray"
                                            >
                                                <use
                                                    xlinkHref={SvgManager.get(
                                                        obj.component.meta.icon
                                                    )}
                                                />
                                            </svg>
                                        </div>
                                        <div className="sector-list__title">
                                            {obj.component.meta.name}
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                    )}
                </div>
            </div>
        );
    }
}
