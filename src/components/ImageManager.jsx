import React, { Component } from "react";
import PropTypes from "prop-types";
import ImageViewer from "./ImageViewer";

import ImageStorage from "../storage/ImageStorage.js";
import { truncateStr } from "../Helpers.js";

const ValidImageTypes = [
    "image/gif",
    "image/jpeg",
    "image/png",
    "image/svg+xml",
    "image/webp"
];

/**
 * Менеджер картинок. Позволяет выбирать новые загруженные и старые картинки
 */
export default class ImageManager extends Component {
    constructor(props) {
        super(props);

        this.state = {
            errorMessages: [],
            files: [],
            selectedImage: null,
            filesContent: []
        };
    }

    componentDidMount() {
        this.getImagesList();
    }

    /**
     * Получаем список картинок
     */
    getImagesList() {
        // Загружаем список uuid'ов изображений
        this.setState({
            files: ImageStorage.getImagesListIds(),
        }, () => this.loadContents());
    }
    /**
     * Загружаем объекты изображений по доступным uuid.
     */
    loadContents() {
        const contents = this.state.files.map(uuid => {
            if (!uuid) {
                return null;
            }
            return ImageStorage.getById(uuid);
        });
        console.warn(contents);
        this.setState({
            filesContent: contents
        });
    }

    /**
     * Добавляем новую картинку
     * @param {String} name название файла
     * @param {HTMLElement} ev
     */
    async addFile(file, ev) {
        const files = this.state.files;
        const newImg = await ImageStorage.createImage(
            file.name,
            file.type,
            ev.target.result
        );
        this.state.filesContent.push(newImg);
        files.push(newImg.id);
        this.setState({ files });
    }

    /**
     * Добавляем новые файлы в менеджер
     * @param {Array} files массив файлов
     */
    openFile(files) {
        const errorMessages = [];

        for (const ind in files) {
            const file = files[ind];
            if (!(file instanceof File)) {
                continue;
            }
            if (!ValidImageTypes.includes(file.type)) {
                errorMessages.push(`Файл ${file.name} не является картинкой`);
                continue;
            }
            var reader = new FileReader();
            reader.onload = this.addFile.bind(this, file);

            reader.readAsDataURL(file);
        }

        this.setState({ errorMessages });
    }

    /**
     * При клике на картинку, выделяем ее.
     * @param {String} name название файла картинки
     * @param {Number} ind
     */
    onClickImage(name, ind) {
        const selectedImage = this.state.filesContent[ind];
        this.setState({ selectedImage });
    }

    checkLimitFiles() {
        const MAX_COUNT_IMAGES = 100;
        const message = `Превышен лимит загрузки файлов ${MAX_COUNT_IMAGES}. Удалите ненужные файлы.`;
        if (this.state.files.length >= MAX_COUNT_IMAGES) {
            return message;
        }
        return null;
    }

    /**
     * Удаляем картинку с менеджера, а также вызываем метод ImageItem::remove, для удаления картинки с ядра
     * @param {String} name название файла картинки
     * @param {Number} ind
     */
    removeImage(name, ind) {
        const files = this.state.files;
        const contents = this.state.filesContent;

        this.state.filesContent[ind].remove();
        files.splice(ind, 1);
        contents.splice(ind, 1);
        this.setState({ files });
    }

    render() {
        const isFulled = this.checkLimitFiles();
        return (
            <div className="image-manager__outer">
                <div className="image-manager">
                    <span
                        className="image-manager__close"
                        onClick={this.props.onCancel}
                    >
                        ×
                    </span>
                    <div className="image-manager__container">
                        <div className="image-manager__files">
                            <label className="button">
                                <input
                                    type="file"
                                    disabled={isFulled}
                                    onChange={e => {
                                        this.openFile(e.target.files);
                                        e.target.value = "";
                                    }}
                                    multiple
                                    className="image-manager__file"
                                />Добавить файлы</label>
                            {this.state.errorMessages.length || isFulled ? (
                                <div className="error-messages">
                                    {[
                                        isFulled,
                                        ...this.state.errorMessages
                                    ].map(mess => (
                                        <p className="error-messages__item">
                                            {mess}
                                        </p>
                                    ))}
                                </div>
                            ) : null}
                            <div className="image-manager__list">
                                {this.state.filesContent.map((img, ind) => (
                                    <div
                                        className={
                                            "image-manager__item " +
                                            (this.state.selectedImage &&
                                            this.state.selectedImage.id ===
                                                img.id
                                                ? "image-manager__item_active"
                                                : "")
                                        }
                                        onClick={this.onClickImage.bind(
                                            this,
                                            img.name,
                                            ind
                                        )}
                                        key={img.id + ind}
                                        data-id={img.id}
                                        title={img.name}
                                    >
                                        <span
                                            className="image-manager__remove"
                                            onClick={e =>
                                                this.removeImage(img.name, ind)
                                            }
                                        >×</span>
                                        <img
                                            src={img.content}
                                            width="100px"
                                            alt={img.name}
                                        />
                                        <h4 className="image-manager__subtitle">
                                            {truncateStr(img.name)}
                                        </h4>
                                    </div>
                                ))}
                            </div>
                        </div>
                        <div className="image-manager__viewer">
                            {this.state.selectedImage ? (
                                <div className="image-manager__viewer-layout">
                                    <ImageViewer
                                        image={this.state.selectedImage}
                                    />
                                    <div className="image-manager__button">
                                        <button
                                            className="button"
                                            onClick={e =>
                                                this.props.onSelect(
                                                    this.state.selectedImage
                                                )
                                            }
                                        >Выбрать</button>
                                        <button
                                            className="button"
                                            onClick={this.props.onCancel}
                                        >Отмена</button>
                                    </div>
                                </div>
                            ) : null}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

ImageManager.propTypes = {
    onCancel: PropTypes.func,
    onSelect: PropTypes.func
};
