import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import { SvgManager } from "../Helpers.js";

function ViewerPanelButton(props) {
    return (
        <div className="viewer-panel__button" onClick={props.onClick}>
            <svg version="1.1" width="32" height="32" viewBox="0 0 64 64">
                <use xlinkHref={SvgManager.get(props.iconName)} />
            </svg>
        </div>
    )
}

/**
 * Нижняя панель для управление зумом холста
 */
export default class ViewerPanel extends PureComponent {
    constructor(props) {
        super(props);
        this.zoomAdd = 0.1; // шаг зума при клике
    }

    render() {
        return (
            <div className="viewer-panel">
                <ViewerPanelButton onClick={() => this.props.onZoom(this.zoomAdd)} iconName="zoom-in" />
                <ViewerPanelButton onClick={() => this.props.onZoom(-this.zoomAdd)} iconName="zoom-out" />
                <ViewerPanelButton onClick={() => this.props.onZoomToSource()} iconName="zoom-original" />
                <ViewerPanelButton onClick={() => this.props.onZoomFill()} iconName="zoom-fill" />
            </div>
        );
    }
}

ViewerPanel.propTypes = {
    onZoom: PropTypes.func,
    onZoomToSource: PropTypes.func,
    onZoomFill: PropTypes.func
};
