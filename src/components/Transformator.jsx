import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { SvgManager } from "../Helpers.js";

/**
 * Точка для ресайза
 */
function ResizePoint(props) {
    return (
        <rect
            onMouseDown={e =>
                props.onMouseDown(
                    props.objectIndex,
                    props.name || `resize_${props.row}${props.cel}`,
                    e
                )
            }
            className={props.name || `resizer_${props.row}${props.cel}`}
            width={props.resizerSize}
            height={props.resizerSize}
            key={`${props.row}${props.cel}`}
            x={props.x}
            y={props.y}
        />
    );
}

/**
 * Отображение всех точек ресайза, по краям объекта
 */
function ResizePointList(props) {
    return ["t", "m", "b"].map((row, y) =>
        ["l", "c", "r"].map(
            (cel, x) =>
                row + cel !== "mc" && (
                    <ResizePoint
                        onMouseDown={props.onMouseDown}
                        objectIndex={props.objectIndex}
                        row={row}
                        cel={cel}
                        x={(x * props.w) / 2 - props.resizerSize / 2}
                        y={(y * props.h) / 2 - props.resizerSize / 2}
                        resizerSize={props.resizerSize}
                        key={row + cel}
                    />
                )
        )
    );
}

/**
 * Кнопка поворота объекта
 */
function Rotator(props) {
    return (
        <g style={{ transform: `scale(${props.scale / 2}) ` }}>
            <use
                xlinkHref={SvgManager.get("rotate")}
                transform={`translate(${(props.w / props.scale) *
                    2}, ${props.h / props.scale / 2})`}
                onMouseDown={props.onMouseDown}
                style={{ pointerEvents: "bounding-box" }}
            />
        </g>
    );
}

/**
 * Кнопка пропорционального масштабирования объекта
 */
function Scaler(props) {
    return (
        <g
            style={{
                transform: `scale(${1 / props.zoom}) translate(${(props.w -
                    10) *
                    props.zoom}px, ${(props.h - 10) * props.zoom}px)`
            }}
        >
            <use
                xlinkHref={SvgManager.get("resize")}
                width="64px"
                style={{
                    cursor: "nwse-resize",
                    pointerEvents: "bounding-box"
                }}
                onMouseDown={props.onMouseDown}
                transform="scale(0.25) rotate(-45 32 32)"
                fill="black"
                stroke="white"
            />
        </g>
    );
}

/**
 * Кнопка блокирования изменения объекта
 */
function Locker(props) {
    return (
        <g
            style={{
                transform: `translate(-10px, -30px) scale(${1 / props.zoom})`
            }}
        >
            <use
                xlinkHref={SvgManager.get(props.locked ? "locked" : "unlocked")}
                fill="orange"
                width="70px"
                x={props.w * props.zoom * 4}
                height="70px"
                viewport="0 0 70 70"
                transform="scale(0.25)"
                onMouseDown={e => e.stopPropagation()}
                onClick={props.onClick}
                style={{ pointerEvents: "bounding-box" }}
            />
        </g>
    );
}

/**
 * Кнопка удаления объекта
 */
function Remover(props) {
    return (
        <g
            style={{
                transform: `translate(-10px, -30px) scale(${1 / props.zoom})`
            }}
        >
            <use
                xlinkHref={SvgManager.get("remove")}
                fill="red"
                width="70px"
                transform="scale(0.25)"
                onMouseDown={e => e.stopPropagation()}
                onClick={props.onClick}
                style={{
                    cursor: "pointer",
                    pointerEvents: "bounding-box"
                }}
            />
        </g>
    );
}
/**
 * Трансформатор объекта. Изменяет размер, поворот и другие параметры объекта
 */
export default class Transformator extends PureComponent {
    render() {
        // Если объект не выделен на холсте, не показываем трансформатор
        if (!(this.props.object && this.props.object.id >= 0)) {
            return <g />;
        }

        let w, h;
        let isScaler = false; // Целевой объект масштабируемый пропорционально
        let isResize = false; // Целевой объект масштабируемый не пропорционально
        let isRotator = false; // Целевой объект поворачивается
        let isVector = false; // Целевой объект это вектор с двумя точками позиции, вместо одной. И он не имеет размеров

        if ("angle" in this.props.object) {
            isRotator = true;
        }

        if ("width" in this.props.object && "height" in this.props.object) {
            isResize = true;
            w = this.props.object.width;
            h = this.props.object.height;
        } else {
            // For scallable object
            w = 64;
            h = 64;
        }

        if ("scale" in this.props.object) {
            w = h = Math.max(w, h) * this.props.object.scale;
            isScaler = true;
            isResize = false;
        }

        if ("x2" in this.props.object) {
            isVector = true;
        }

        const resizerSize = 6 / this.props.zoom;
        const strokeWidth = resizerSize / 6;
        const objectIndex = this.props.objectIndex;

        return (
            <g
                className="transformator"
                transform={`translate(${this.props.object.x},${
                    this.props.object.y
                })`}
            >
                <g
                    className="transformator-item"
                    transform={`rotate(${this.props.object.angle || 0} ${w /
                        2} ${h / 2})`}
                >
                    <rect
                        fill="transparent"
                        width={w}
                        height={h}
                        style={{ pointerEvents: "none" }}
                    />

                    {isRotator && !this.props.object.locked && (
                        <Rotator
                            scale={this.props.object.scale}
                            onMouseDown={e =>
                                this.props.onMouseDown(
                                    this.props.objectIndex,
                                    "rotate",
                                    e
                                )
                            }
                            w={w}
                            h={h}
                        />
                    )}
                    <g
                        transform={`rotate(${-this.props.object.angle ||
                            0} ${w / 2} ${h / 2})`}
                    >
                        {isScaler && !this.props.object.locked && (
                            <Scaler
                                zoom={this.props.zoom}
                                w={w}
                                h={h}
                                onMouseDown={e =>
                                    this.props.onMouseDown(
                                        this.props.objectIndex,
                                        "scale",
                                        e
                                    )
                                }
                            />
                        )}
                        <Locker
                            zoom={this.props.zoom}
                            locked={this.props.object.locked}
                            w={w}
                            h={h}
                            onClick={e =>
                                this.props.onToggleLock(this.props.objectIndex)
                            }
                        />

                        {!this.props.object.locked && (
                            <Remover
                                zoom={this.props.zoom}
                                onClick={e => {
                                    this.props.onDeleteObject(
                                        this.props.object
                                    );
                                }}
                            />
                        )}
                    </g>
                    {isResize && !this.props.object.locked && (
                        <g
                            fill="white"
                            stroke="black"
                            strokeWidth={strokeWidth}
                            onMouseUp={e => this.props.onChangeEnd(objectIndex)}
                        >
                            {/* Выводим точки ресайза */}
                            <ResizePointList
                                onMouseDown={this.props.onMouseDown}
                                w={w}
                                h={h}
                                resizerSize={resizerSize}
                                objectIndex={objectIndex}
                            />
                        </g>
                    )}
                    {/* Если объект масштабируется пропорционально, то показываем только одну точку */}
                    {isVector && !this.props.object.locked && (
                        <g
                            fill="white"
                            stroke="black"
                            strokeWidth={strokeWidth}
                        >
                            <ResizePoint
                                name="vector_start"
                                objectIndex={objectIndex}
                                onMouseDown={this.props.onMouseDown}
                                x={this.props.object.x1 - resizerSize / 2}
                                y={this.props.object.y1 - resizerSize / 2}
                                resizerSize={resizerSize}
                            />
                            <ResizePoint
                                name="vector_end"
                                objectIndex={objectIndex}
                                onMouseDown={this.props.onMouseDown}
                                x={this.props.object.x2 - resizerSize / 2}
                                y={this.props.object.y2 - resizerSize / 2}
                                resizerSize={resizerSize}
                            />
                        </g>
                    )}
                </g>
            </g>
        );
    }
}

Transformator.propTypes = {
    object: PropTypes.object,
    zoom: PropTypes.number,
    objectIndex: PropTypes.number,
    onMouseDown: PropTypes.func,
    onToggleLock: PropTypes.func,
    onDeleteObject: PropTypes.func,
    onChangeEnd: PropTypes.func
};
