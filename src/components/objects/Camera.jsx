import React from "react";
import Fields from "../fields";
import defaultProperties from "../DefaultProperties";
import BaseComponent from "./BaseComponent";
import TextBox from "../TextBox";
import JoinLine from "../JoinLine";
import { SvgManager } from "../../Helpers.js";

/**
 * Объект - камера. Основной объект на холсте. Показывает состояние, обзор, тревожность камеры.
 */
export default class CameraComponent extends BaseComponent {
    static meta = {
        properties: Object.assign({}, defaultProperties, {
            source: {
                title: "Источник",
                type: Fields.ListField,
                arguments: { excludes: "AudioStream" },
                parcel: "streams",
                hideElementIfNone: true
            },
            icon: {
                title: "Иконка",
                type: Fields.IconSelectField,
                icons: ["camera", "camerafish", "cameraptz"],
                defaultValue: "camera"
            },
            viewRadius: {
                title: "Угол обзора",
                type: Fields.RangeField,
                minValue: 6,
                maxValue: 360,
                defaultValue: 60
            },
            viewDistance: {
                title: "Дальность обзора",
                type: Fields.RangeField,
                maxValue: 500,
                defaultValue: 0
            },
            viewLength: {
                title: "Длинна обзора",
                type: Fields.RangeField,
                maxValue: 1000,
                minValue: 0,
                defaultValue: 150
            },
            viewSectors: {
                title: "Сектора",
                type: Fields.SectorListField,
                defaultValue: []
            },
            strokeWidth: null,
            fill: null,
            action: null,
            stroke: null
        }),
        icon: "camera",
        title: "Камера",
        name: "Camera"
    }

    // Цвета камеры по состоянию источника
    static statusColor = {
        Disabled: "#4020a0", // Отключена
        Fault: "#e7201a", // Недоступна
        OK: "darkgray", // Онлайн
        "OK-rec": "#3da23d" // Онлайн и записывает
    };

    render() {
        let centerX = 32;
        let centerY = 32;
        const titleFontSize = '' + this.props.object.fontSize;

        // У камеры может быть два источника, получаем состояние каждого из них
        let primaryBack =
            CameraComponent.statusColor[
                this.props.object.sourceObject.primaryStatus
            ] || "darkgray";
        let secondaryBack =
            CameraComponent.statusColor[
                this.props.object.sourceObject.secondaryStatus
            ] || primaryBack;
        let isSingleStream = false;
        // Если источник один, то цвет заливать будем всю камеру
        if (!this.props.object.sourceObject.secondaryStatus) {
            isSingleStream = true;
        }

        return (
            <g>
                {/* Применяем смещение объекта */}
                <g
                    transform={`translate(${this.props.object.x} ${this.props.object.y})`}
                    className="object-transition"
                >
                    {/* Применяем поворот объекта, а также непрозрачность */}
                    <g
                        style={{ opacity: this.props.object.opacity / 100 }}
                        transform={`rotate(${
                            this.props.object.angle
                        } ${centerX * this.props.object.scale} ${centerY *
                            this.props.object.scale})`}
                    >
                        {/* Применяем масштабирование объекта */}
                        <g transform={`scale(${this.props.object.scale})`}>
                            {/* Устанавливаем маску для заливки камеры. Иконка камеры делится пополам, и для левой части заливается первый стрим, а для правой второй */}
                            <g
                                clipPath={`url(#${this.props.object.icon}-clip-${this.props.canvasId})`}
                                mask={`url(#${this.props.object.icon}-clip-${this.props.canvasId})`}
                            >
                                {/* Поворачиваем данную группу в противоположную сторону, чтобы компенсировать поворот. Заливка не должна поворачиваться */}
                                <g
                                    ref={this.props.object.ref}
                                    transform={`rotate(${-this.props.object.angle} ${centerX} ${centerY})`}
                                >
                                    {/* Левая часть заливки для первого стрима */}
                                    <rect x={16} y={-2} width={32} height={68} fill={isSingleStream ? primaryBack : "black" } />
                                    {/* Разделитель частей заливки */}
                                    <rect x={-2} y={-2} width={34} height={68} fill={primaryBack} />
                                    {/* Правая часть заливки для второго стрима */}
                                    <rect x={32} y={-2} width={34} height={68} fill={secondaryBack} />
                                </g>
                            </g>
                            {/* Отображаем иконку камеры */}
                            <use
                                xlinkHref={SvgManager.get(
                                    this.props.object.icon
                                )}
                                stroke="#5d5c5c"
                                strokeWidth="2"
                                fill="transparent"
                                style={{ pointerEvents: "bounding-box" }}
                            />
                        </g>
                    </g>
                    {/* Показываем текстовое окошко с текстом информации об источнике */}
                    {this.props.object.showText &&
                        this.props.object.sourceObject.text &&
                        this.props.object.sourceObject.text.length > 0 && (
                            <g
                                stroke="white"
                                style={{
                                    transform: `translate(${(64 *
                                        this.props.object.scale) /
                                        2 -
                                        175}px, ${80 *
                                        this.props.object.scale}px)`,
                                    pointerEvents: "none"
                                }}
                            >
                                <TextBox
                                    className={
                                        this.props.isAlarm
                                            ? "alarm-animation-background"
                                            : ""
                                    }
                                    width="400"
                                    x="40"
                                    y="0"
                                    fontSize={titleFontSize}
                                >
                                    {this.props.object.sourceObject.text}
                                </TextBox>
                            </g>
                        )}
                </g>
                {/* Показываем дочерние сегменты (фигуры) камеры */}
                {this.props.object.childs &&
                    this.props.object.childs.map((child, ind) => (
                        <JoinLine
                            parent={this.props.object}
                            child={child}
                            key={ind}
                            parentCenter={{ x: centerX, y: centerY }}
                        />
                    ))}
            </g>
        );
    }
}
