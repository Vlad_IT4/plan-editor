import CanvasPlan from "./CanvasPlan";
import CameraComponent from "./Camera";
import ImageComponent from "./Image";
import CircleComponent from "./Circle";
import RectangleComponent from "./Rectangle";
import PathComponent from "./Path";
import ArrowComponent from "./Arrow";
import LineComponent from "./Line";
import MicroPhoneComponent from "./Microphone";
import ButtonComponent from "./Button";
import WebPageComponent from "./WebPage";
import TourComponent from "./Tour";
import SalvoComponent from "./Salvo";

// Доступные объекты
export default {
    CanvasPlan,
    CameraComponent,
    MicroPhoneComponent,
    ButtonComponent,
    TourComponent,
    SalvoComponent,
    WebPageComponent,
    ImageComponent,
    CircleComponent,
    RectangleComponent,
    PathComponent,
    ArrowComponent,
    LineComponent
};
