import React from "react";
import Fields from "../fields";
import defaultProperties from "../DefaultProperties";
import BaseComponent from "./BaseComponent";
import TextBox from "../TextBox";
import { SvgManager } from "../../Helpers.js";

/**
 * Объект - изображение
 */
export default class ImageComponent extends BaseComponent {
    static meta = {
        properties: Object.assign({}, defaultProperties, {
            image: {
                title: "Изображение",
                type: Fields.ImageField
            },
            fill: null,
            viewRadius: null,
            stroke: null,
            strokeWidth: null,
            scale: null,
            width: {
                title: "Ширина",
                type: Fields.Field,
                defaultValue: 48,
                minValue: 1
            },
            height: {
                title: "Высота",
                type: Fields.Field,
                defaultValue: 50,
                minValue: 1
            },
            angle: null
        }),
        icon: "image",
        title: "Изображение",
        name: "Image"
    };

    constructor(props) {
        super(props);
        this.oldImageUuid = this.getImageUuidByImage(this.props.object.image);
    }

    /**
     * Получаем uuid изображения
     * @param {Object | String} image
     */
    getImageUuidByImage(image) {
        if (image) {
            if (typeof image === "string") {
                return image;
            } else {
                return image.id;
            }
        }
    }

    componentDidUpdate(prevProps, prevState) {
        // нужно при изменении картинки, вызывать onLoadImage
        // но т.к. ссылка на object не меняется, а меняются только поля
        // нужно кешировать id текущей картинки, и при изменении итерации объекта
        // сравнивать ее с id новой картинки
        const imageUuid = this.getImageUuidByImage(this.props.object.image);
        if (this.oldImageUuid !== imageUuid) {
            this.props.onLoadImage(this.props.object);
            this.oldImageUuid = imageUuid;
        }
        return true;
    }

    /**
     * Обновляем стартовые размеры объекта. Они пригодятся для пропорционального масштабирования
     */
    componentWillMount() {
        if (!(
                this.props.object.startWidth === undefined ||
                this.props.object.startHeight === undefined
            )
        ) {
            return;
        }
        this.props.object.startWidth = this.props.object.width;
        this.props.object.startHeight = this.props.object.height;
    }

    /**
     * Получаем содержимое изображения
     */
    getImage() {
        if (this.props.object.image && typeof this.props.object.image.content) {
            return this.props.object.image.content;
        }
    }

    /**
     * Считаем масштабирование картинки, которое является частным от текущих размеров к начальным
     */
    getImageScale() {
        if (
            this.props.object.width === null ||
            this.props.object.height === null
        ) {
            return "scale(1)";
        }

        return `scale(${this.props.object.width /
            this.props.object.startWidth} ${this.props.object.height /
            this.props.object.startHeight})`;
    }

    render() {
        let image = this.getImage();
        const titleFontSize = this.props.object.fontSize;

        return (
            <g
                transform={`translate(${this.props.object.x} ${
                    this.props.object.y
                })`}
                className="object-transition"
            >
                <g
                    style={{ opacity: this.props.object.opacity / 100 }}
                    transform={this.getImageScale()}
                    onClick={() =>
                        this.onAction({
                            object: this.props.object.source,
                            name: "plans"
                        })
                    }
                >
                    {image ? (
                        <image xlinkHref={image} ref={this.props.object.ref} />
                    ) : (
                        <use
                            xlinkHref={SvgManager.get("image")}
                            ref={this.props.object.ref}
                            width="64px"
                            height="64px"
                        />
                    )}
                </g>
                {/* Показываем текст источника */}
                {this.props.object.showText &&
                    this.props.object.sourceObject.text &&
                    this.props.object.sourceObject.text.length > 0 && (
                        <g
                            stroke="white"
                            style={{
                                transform: `translate(${this.props.object
                                    .width /
                                    2 -
                                    175}px, ${this.props.object.height}px)`,
                                pointerEvents: "none"
                            }}
                        >
                            <TextBox
                                width="400"
                                x="40"
                                y="0"
                                fontSize={titleFontSize}
                            >
                                {this.props.object.sourceObject.text}
                            </TextBox>
                        </g>
                    )}
            </g>
        );
    }
}
