import React from "react";
import defaultProperties from "../DefaultProperties";
import Fields from "../fields";
import BaseComponent from "./BaseComponent";
import TextBox from "../TextBox";
import { SvgManager } from "../../Helpers.js";

/**
 * Объект - тур, при клике на этот объект, в окне камер должет появится объект, который перелистывает камеры (слайдшоу)
 */
export default class TourComponent extends BaseComponent {
    static meta = {
        properties: Object.assign({}, defaultProperties, {
            source: {
                title: "Тур",
                type: Fields.ListField,
                parcel: "tours",
                hideElementIfNone: true
            },
            fill: null,
            stroke: null,
            angle: null,
            action: null
        }),
        icon: "tour",
        title: "Тур",
        name: "Tour",
        isPreventScroll: true
    };

    render() {
        const titleFontSize = '' + this.props.object.fontSize;

        let countText = "";
        if (
            this.props.object.sourceObject &&
            this.props.object.sourceObject.count
        ) {
            // не даем вывести трех значное число, пишем +99
            if (this.props.object.sourceObject.count >= 100) {
                countText = "+99";
            } else {
                countText = "x" + this.props.object.sourceObject.count;
            }
        }
        return (
            <g
                transform={`translate(${this.props.object.x} ${
                    this.props.object.y
                })`}
                className="object-transition"
            >
                <g
                    style={{ opacity: this.props.object.opacity / 100 }}
                    transform={`scale(${this.props.object.scale})`}
                    onClick={e =>
                        this.onAction({
                            object: this.props.object.source,
                            name: "tour:open"
                        })
                    }
                >
                    {/* Показываем иконку */}
                    <use
                        xlinkHref={SvgManager.get("tour")}
                        ref={this.props.object.ref}
                    />
                    {/* Показываем кол-во слайдов в туре */}
                    <text
                        fontSize="22"
                        transform="translate(32 48)"
                        textAnchor="middle"
                    >
                        {countText}
                    </text>
                </g>
                {/* Показываем текст */}
                {this.props.object.showText &&
                    this.props.object.sourceObject.text &&
                    this.props.object.sourceObject.text.length > 0 && (
                        <g
                            stroke="white"
                            style={{
                                transform: `translate(${(64 * this.props.object.scale) / 2 - 175}px, ${80 * this.props.object.scale}px)`,
                                pointerEvents: "none"
                            }}
                        >
                            <TextBox
                                width="400"
                                x="40"
                                y="0"
                                fontSize={titleFontSize}
                            >
                                {this.props.object.sourceObject.text}
                            </TextBox>
                        </g>
                    )}
            </g>
        );
    }
}
