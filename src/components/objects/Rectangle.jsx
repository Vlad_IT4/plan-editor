import React from "react";
import defaultProperties from "../DefaultProperties";
import Fields from "../fields";
import BaseComponent from "./BaseComponent";
import TextBox from "../TextBox";

/**
 * Объект - фигура, прямоугольник
 */
export default class RectangleComponent extends BaseComponent {
    static meta = {
        properties: Object.assign({}, defaultProperties, {
            scale: null,
            width: {
                title: "Ширина",
                type: Fields.Field,
                defaultValue: 100,
                minValue: 1
            },
            height: {
                title: "Высота",
                type: Fields.Field,
                defaultValue: 100,
                minValue: 1
            },
            angle: null
        }),
        icon: "rect",
        title: "Прямоугольник",
        name: "Rectangle",
        isShape: true
    };

    render() {
        let fill = this.props.object.fill;
        let stroke = this.props.object.stroke;
        let opacity = this.props.object.opacity / 100;

        let source, cursor;
        const classNames = [];

        // Если данный объект является дочерним другому объекту, то получаем источника, и задаем стили
        if (this.props.object.parentObject) {
            source = this.props.object.parentObject.source;
            classNames.push("observ-area");
            fill = null;
            stroke = null;
            cursor = this.props.isEditor ? "default" : "crosshair";
        }

        // Добавляем анимацию "мигание", если объект находится в тревоге
        if (this.props.isAlarm) {
            classNames.push("alarm-animation-fill");
        }
        const titleFontSize = this.props.object.fontSize;
        return (
            <g
                fill={fill}
                stroke={stroke}
                strokeWidth={this.props.object.strokeWidth}
                transform={`translate(${this.props.object.x} ${
                    this.props.object.y
                })`}
                className="object-transition"
            >
                {/* Рисуем прямоугольник */}
                <rect
                    className={classNames.join(" ")}
                    data-object={source}
                    width={this.props.object.width}
                    height={this.props.object.height}
                    onClick={e =>
                        this.onAction({
                            object: this.props.object.source,
                            name: "plans"
                        })
                    }
                    x="0"
                    y="0"
                    style={{ cursor, opacity }}
                    ref={this.props.object.ref}
                />
                {/* Рисуем текст */}
                {this.props.object.showText &&
                    this.props.object.sourceObject.text &&
                    this.props.object.sourceObject.text.length > 0 && (
                        <g
                            stroke="white"
                            style={{
                                transform: `translate(${this.props.object.width / 2 - 175}px, ${this.props.object.height}px)`,
                                pointerEvents: "none"
                            }}
                        >
                            <TextBox
                                width="400"
                                x="40"
                                y="0"
                                fontSize={titleFontSize}
                            >
                                {this.props.object.sourceObject.text}
                            </TextBox>
                        </g>
                    )}
            </g>
        );
    }
}
