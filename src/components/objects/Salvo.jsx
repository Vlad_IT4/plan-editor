import React from "react";
import defaultProperties from "../DefaultProperties";
import Fields from "../fields";
import BaseComponent from "./BaseComponent";
import TextBox from "../TextBox";
import { SvgManager } from "../../Helpers.js";

/**
 * Объект - залп, при клике на этот объект, в окне камер должны открыться множество указанных камер
 */
export default class SalvoComponent extends BaseComponent {
    static meta = {
        properties: Object.assign({}, defaultProperties, {
            source: {
                title: "Залп",
                type: Fields.ListField,
                parcel: "salvos",
                hideElementIfNone: true
            },
            fill: null,
            stroke: null,
            angle: null,
            action: null
        }),
        icon: "salvo",
        title: "Залп",
        name: "Salvo",
        isPreventScroll: true,
        isPreventDrag: true
    };

    render() {
        const titleFontSize = '' + this.props.object.fontSize;

        return (
            <g
                transform={`translate(${this.props.object.x} ${
                    this.props.object.y
                })`}
                className="object-transition"
            >
                <g
                    transform={`scale(${this.props.object.scale})`}
                    style={{ opacity: this.props.object.opacity / 100 }}
                >
                    {/* Рисуем иконку */}
                    <use
                        xlinkHref={SvgManager.get("salvo")}
                        ref={this.props.object.ref}
                        onClick={e =>
                            this.onAction({
                                object: this.props.object.source,
                                name: "salvo:open"
                            })
                        }
                    />
                </g>
                {/* Рисуем текст */}
                {this.props.object.showText &&
                    this.props.object.sourceObject.text &&
                    this.props.object.sourceObject.text.length > 0 && (
                        <g
                            stroke="white"
                            style={{
                                transform: `translate(${(64 * this.props.object.scale) / 2 - 175}px, ${80 * this.props.object.scale}px)`,
                                pointerEvents: "none"
                            }}
                        >
                            <TextBox
                                width="400"
                                x="40"
                                y="0"
                                fontSize={titleFontSize}
                            >
                                {this.props.object.sourceObject.text}
                            </TextBox>
                        </g>
                    )}
            </g>
        );
    }
}
