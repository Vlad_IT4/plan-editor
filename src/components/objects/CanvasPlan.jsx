import Fields from "../fields";

export default class CanvasPlan {
    static meta = {
        properties: {
            name: {
                title: "Название",
                type: Fields.TextField
            },
            shortcut: {
                title: "Ярлык",
                type: Fields.NumberField,
                defaultValue: 0
            },
            width: {
                title: "Ширина",
                type: Fields.NumberField,
                minValue: 1
            },
            height: {
                title: "Высота",
                type: Fields.NumberField,
                minValue: 1
            },
            zIndex: null,
            backgroundImage: {
                title: "Фоновое изображение",
                type: Fields.ImageField
            },
            backgroundAlign: {
                title: "Выравнивание фона",
                type: Fields.AlignField,
                defaultValue: "xMinYMin"
            },
            backgroundSize: {
                title: "Размер изображения",
                type: Fields.ListField,
                parcel: "backgroundSize",
                defaultValue: true,
                hideEmpty: true
            },
            filter: {
                title: "Наложение фильтра",
                type: Fields.ListField,
                parcel: "imageFilters"
            }
        },
        name: "CanvasPlan"
    };
}
