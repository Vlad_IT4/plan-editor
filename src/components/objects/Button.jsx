import React from "react";
import defaultProperties from "../DefaultProperties";
import Fields from "../fields";
import BaseComponent from "./BaseComponent";
import TextBox from "../TextBox";

/**
 * Объект - кнопка
 */
export default class ButtonComponent extends BaseComponent {
    static meta = {
        properties: Object.assign({}, defaultProperties, {
            source: {
                title: "Макрокнопка",
                type: Fields.ListField,
                parcel: "macroses",
                hideElementIfNone: true // прячем объект, если не задан источник (действие кнопки)
            },
            fill: null,
            stroke: null,
            angle: null,
            action: null,
            strokeWidth: null
        }),
        icon: "button",
        title: "Кнопка",
        name: "Button",
        isPreventScroll: true, // Предотвращаем скролл при зажатой клавиши мыши по кнопке
        isPreventDrag: true // Запрещаем drag&drop
    };

    /**
     * Перерисовываем также при изменении состояния "нажатости" кнопки
     */
    shouldComponentUpdate(n, state) {
        if (super.shouldComponentUpdate(n)) {
            return true;
        }

        return state.pushed !== this.state.pushed;
    }

    /**
     * Выполнение действия по нажатию на кнопку
     * @param {Object} obj целевой объект
     */
    onAction(obj) {
        if (this.props.onAction && !this.props.isEditor) {
            this.props.onAction(obj);
        }
    }

    componentDidMount() {
        document.addEventListener("mouseup", this.handleMouseupOutside);
    }

    componentWillUnmount() {
        document.removeEventListener("mouseup", this.handleMouseupOutside);
    }

    /**
     * "Отпускаем" кнопку при mouseup за пределами самой кнопки
     */
    handleMouseupOutside = () => {
        this.setState({ pushed: false });
    };

    render() {
        // Получаем текущее состояние кнопки
        const pushed = this.props.object.mode === "pushed" || this.state.pushed;
        const titleFontSize = '' + this.props.object.fontSize;
        return (
            <g
                transform={`translate(${this.props.object.x} ${
                    this.props.object.y
                })`}
                className="object-transition"
            >
                <g
                    style={{ opacity: this.props.object.opacity / 100 }}
                    onMouseDown={e => {
                        this.setState({ pushed: true && !this.props.isEditor });
                    }}
                    onMouseUp={() => {
                        this.setState({
                            pushed: false && !this.props.isEditor
                        });
                        this.onAction({
                            object: this.props.object.source,
                            name: "macros:click"
                        });
                    }}
                >
                    <g
                        transform={`scale(${this.props.object.scale})`}
                        ref={this.props.object.ref}
                    >
                        <ellipse
                            cx="32.208"
                            cy="39.31"
                            rx="30.65"
                            ry="23.575"
                            fill="black"
                            strokeWidth=".18854"
                        />
                        <ellipse
                            cx="32.208"
                            cy="39.31"
                            rx="30.65"
                            ry="23.575"
                            fill="#b2b2b2"
                            opacity=".6"
                            strokeWidth=".18854"
                        />
                        <ellipse
                            cx="32.219"
                            cy="35.78"
                            rx="30.65"
                            ry="23.575"
                            fill="#b2b2b2"
                            strokeWidth=".18854"
                        />
                        <rect
                            x="7.0553"
                            y="20.94"
                            width="50.952"
                            height="12.741"
                            strokeWidth=".16141"
                            style={{
                                animationDuration: "0.1s",
                                animationName: pushed ? "pushdown" : "pushup",
                                opacity: +!pushed
                            }}
                        />
                        <ellipse
                            cx="32.3"
                            cy="34.11"
                            rx="25.477"
                            ry="19.859"
                            strokeWidth=".17857"
                        />
                        <g fill={pushed ? "#3fbcff" : "#9eb8ba"} opacity=".8">
                            <rect
                                x="7.0553"
                                y="20.94"
                                width="50.952"
                                height="12.741"
                                strokeWidth=".16141"
                                style={{
                                    animationDuration: "0.1s",
                                    animationName: pushed
                                        ? "pushdown"
                                        : "pushup",
                                    opacity: +!pushed
                                }}
                            />
                            <ellipse
                                cx="32.3"
                                cy="34.11"
                                rx="25.477"
                                ry="19.859"
                                strokeWidth=".17857"
                            />
                        </g>
                        <ellipse
                            style={{
                                transition: "transform 0.1s ease-out",
                                transform: `translateY(${pushed ? 14 : 0}px)`
                            }}
                            cx="32.3"
                            cy="20.24"
                            rx="25.477"
                            ry="19.859"
                            fill={pushed ? "#3fbcff" : "#9eb8ba"}
                            strokeWidth=".17857"
                        />
                    </g>
                </g>
                {this.props.object.showText &&
                    this.props.object.sourceObject.text &&
                    this.props.object.sourceObject.text.length > 0 && (
                        <g
                            stroke="white"
                            style={{
                                transform: `translate(${(64 *
                                    this.props.object.scale) /
                                    2 -
                                    175}px, ${80 * this.props.object.scale}px)`,
                                pointerEvents: "none"
                            }}
                        >
                            <TextBox
                                width="400"
                                x="40"
                                y="0"
                                fontSize={titleFontSize}
                            >
                                {this.props.object.sourceObject.text}
                            </TextBox>
                        </g>
                    )}
            </g>
        );
    }
}
