import React from "react";
import LineComponent from "./Line";

/**
 * Объект - фигура, стрелка
 */
export default class ArrowComponent extends LineComponent {
    // Meta информация каждого объекта
    static meta = {
        properties: LineComponent.meta.properties,
        icon: "arrow", // иконка из svg набора
        title: "Стрелка",
        name: "Arrow",
        isShape: true // это статичная фигура, значит она может быть дочерний фигурой для сегментов камер
    };

    /**
     * Рисуем стрелку
     * @param {*} classNames список классов для тега path
     * @param {*} source источник. Задается, если данный объект является дочерним к другому объекту.
     */
    renderBeforeLine(classNames, source) {
        const object = this.props.object;
        const angle = Math.atan2(object.y2 - object.y1, object.x2 - object.x1);
        const radian = angle * (180 / Math.PI);
        const size =
            (this.props.object.lineHeight + this.props.object.strokeWidth) * 3;

        return (
            <g
                transform={`translate(${object.x1 - size / 2} ${object.y1 -
                    size / 2})`}
                strokeWidth={this.props.object.strokeWidth / 2}
            >
                <g
                    transform={`rotate(${radian + 180} ${size / 2} ${size /
                        2})`}
                >
                    <path
                        className={classNames.join(" ")}
                        data-object={source}
                        d={`M 0 0 L ${size} ${size / 2} L 0 ${size} z`}
                        fill="inherit"
                        stroke="inherit"
                    />
                </g>
            </g>
        );
    }
}
