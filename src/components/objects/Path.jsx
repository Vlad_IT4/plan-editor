import React from "react";
import defaultProperties from "../DefaultProperties";
import Fields from "../fields";
import PathEditor from "../editors/PathEditor";
import BaseComponent from "./BaseComponent";
import TextBox from "../TextBox";

/**
 * Объект - произвольная фигура
 */
export default class PathComponent extends BaseComponent {
    static meta = {
        properties: Object.assign({}, defaultProperties, {
            scale: null,
            width: {
                title: "Ширина",
                type: Fields.Field,
                defaultValue: 100,
                minValue: 1
            },
            height: {
                title: "Высота",
                type: Fields.Field,
                defaultValue: 100,
                minValue: 1
            },
            angle: null
        }),
        editor: PathEditor,
        icon: "figure",
        title: "Фигура",
        name: "Path",
        isShape: true
    }

    /**
     * Обновляем стартовые размеры объекта. Они пригодятся для пропорционального масштабирования
     */
    componentWillMount() {
        if (this.props.object.startWidth && this.props.object.startHeight)
            return;
        this.props.object.startWidth = this.props.object.width;
        this.props.object.startHeight = this.props.object.height;
    }

    render() {
        const points = this.props.object.points.map((point, ind) => {
            const modifer = ind === 0 ? "M" : "L";
            return `${modifer} ${point.x} ${point.y}`;
        });

        let fill = this.props.object.fill;
        let stroke = this.props.object.stroke;
        let opacity = this.props.object.opacity / 100;

        let source, cursor;
        const classNames = [];

        // Если данный объект является дочерним другому объекту, то получаем источника, и задаем стили
        if (this.props.object.parentObject) {
            source = this.props.object.parentObject.source;
            classNames.push("observ-area");
            fill = null;
            stroke = null;
            cursor = this.props.isEditor ? "default" : "crosshair";
        }

        // Добавляем анимацию "мигание", если объект находится в тревоге
        if (this.props.isAlarm) {
            classNames.push("alarm-animation-fill");
        }
        const titleFontSize = this.props.object.fontSize;
        return (
            <g
                style={{ cursor }}
                fill={fill}
                stroke={stroke}
                strokeWidth={this.props.object.strokeWidth}
                transform={`translate(${this.props.object.x} ${
                    this.props.object.y
                })`}
                className="object-transition"
            >
                <g
                    style={{ opacity }}
                    transform={`scale(${this.props.object.width /
                        this.props.object.startWidth} ${this.props.object
                        .height / this.props.object.startHeight})`}
                >
                    {/* Рисуем путь на основе массива точек points */}
                    <path
                        className={classNames.join(" ")}
                        onClick={e =>
                            this.onAction({
                                object: this.props.object.source,
                                name: "plans"
                            })
                        }
                        data-object={source}
                        d={points.join(" ") + " Z"}
                        ref={this.props.object.ref}
                    />
                </g>
                {/* Рисуем текст */}
                {this.props.object.showText &&
                    this.props.object.sourceObject.text &&
                    this.props.object.sourceObject.text.length > 0 && (
                        <g
                            stroke="white"
                            style={{
                                transform: `translate(${this.props.object
                                    .width /
                                    2 -
                                    175}px, ${this.props.object.height}px)`,
                                pointerEvents: "none"
                            }}
                        >
                            <TextBox
                                width="400"
                                x="40"
                                y="0"
                                fontSize={titleFontSize}
                            >
                                {this.props.object.sourceObject.text}
                            </TextBox>
                        </g>
                    )}
            </g>
        );
    }
}
