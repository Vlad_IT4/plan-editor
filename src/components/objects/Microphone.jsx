import React from "react";
import Fields from "../fields";
import defaultProperties from "../DefaultProperties";
import BaseComponent from "./BaseComponent";
import TextBox from "../TextBox";
import JoinLine from "../JoinLine";
import { SvgManager } from "../../Helpers.js";

/**
 * Объект - Микрофон. Показывает состояние, обзор, тревожность аудио источника.
 */
export default class MicroPhoneComponent extends BaseComponent {
    static meta = {
        properties: Object.assign({}, defaultProperties, {
            source: {
                title: "Источник",
                type: Fields.ListField,
                arguments: { includes: "AudioStream" },
                parcel: "streams",
                hideElementIfNone: true
            },
            icon: {
                title: "Иконка",
                type: Fields.IconSelectField,
                icons: ["microphone", "microphone-voice", "bug"],
                defaultValue: "microphone"
            },
            viewRadius: null,
            viewDistance: null,
            viewLength: {
                title: "Длинна обзора",
                type: Fields.RangeField,
                maxValue: 1000,
                minValue: 0,
                defaultValue: 150
            },
            viewSectors: {
                title: "Сектора",
                type: Fields.SectorListField,
                defaultValue: []
            },
            strokeWidth: null,
            fill: null,
            stroke: null,
            action: null
        }),
        icon: "microphone",
        title: "Микрофон",
        name: "MicroPhone"
    };

    // Цвета микрофона по состоянию источника
    static statusColor = {
        Disabled: "#4020a0", // Отключена
        Fault: "#e7201a", // Недоступна
        OK: "darkgray", // Онлайн
        "OK-rec": "#3da23d" // Онлайн и записывает
    };

    render() {
        let centerX = 32;
        let centerY = 32;

        const object = this.props.object;
        const titleFontSize = '' + object.fontSize;
        let primaryBack = 
            MicroPhoneComponent.statusColor[object.sourceObject.primaryStatus] || "darkgray";

        return (
            <g>
                <g
                    fill={primaryBack}
                    transform={`translate(${object.x} ${object.y})`}
                    className="object-transition"
                >
                    <g
                        style={{ opacity: object.opacity / 100 }}
                        transform={`rotate(${object.angle} ${centerX *
                            object.scale} ${centerY * object.scale})`}
                    >
                        <g transform={`scale(${object.scale})`}>
                            {/* Показываем иконку микрофона */}
                            <use
                                xlinkHref={SvgManager.get(object.icon)}
                                stroke="#5d5c5c"
                                strokeWidth="2"
                                ref={object.ref}
                                width="64px"
                                height="64px"
                                style={{ pointerEvents: "bounding-box" }}
                            />
                        </g>
                    </g>
                    {/* Показываем текст */}
                    {object.showText &&
                        object.sourceObject.text &&
                        object.sourceObject.text.length > 0 && (
                            <g
                                stroke="white"
                                style={{
                                    transform: `translate(${(64 *
                                        object.scale) /
                                        2 -
                                        175}px, ${80 * object.scale}px)`,
                                    pointerEvents: "none"
                                }}
                            >
                                <TextBox
                                    width="400"
                                    x="40"
                                    y="0"
                                    fontSize={titleFontSize}
                                >
                                    {object.sourceObject.text}
                                </TextBox>
                            </g>
                        )}
                </g>
                {/* Показываем дочерние сегменты */}
                {object.childs &&
                    object.childs.map((child, ind) => (
                        <JoinLine
                            parent={object}
                            child={child}
                            key={ind}
                            parentCenter={{ x: centerX, y: centerY }}
                        />
                    ))}
            </g>
        );
    }
}
