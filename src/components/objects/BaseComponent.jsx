import { Component } from "react";
import PropTypes from "prop-types";

/**
 * Базовый класс объектов
 */
export default class BaseComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mode: ""
        };
    }

    /**
     * Передаем выполнение действия по объекту
     */
    onAction(obj) {
        if (!this.props.isEditor && this.props.onAction && obj && obj.object) {
            this.props.onAction(obj);
        }
    }

    /**
     * Перерисовываем объект только в том случае, если поменяется число итерации объекта
     * или объект поменяет состояние "тревожности"
     */
    shouldComponentUpdate(n) {
        const p = this.props;

        if (p.iteration !== n.iteration) {
            return true;
        }

        if (p.isAlarm !== n.isAlarm) {
            return true;
        }

        return false;
    }

    /**
     * Фильтр функции JSON.strinify. Игнорирует лишние поля при сериализации объекта
     */
    jsonParseFilter(key, value) {
        if (["ref", "sourceObject", "childs", "parentObject"].includes(key)) {
            return;
        }
        if (key === "component") {
            return value.meta.name + "Component";
        }
        return value;
    }

    /**
     * Сериализация компонента для его сохранения
     */
    toJson() {
        return JSON.stringify(this.object, this.jsonParseFilter);
    }
}

BaseComponent.propTypes = {
    isEditor: PropTypes.bool,
    onAction: PropTypes.func,
    object: PropTypes.object
};
