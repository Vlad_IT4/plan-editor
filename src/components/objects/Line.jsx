import React from "react";
import defaultProperties from "../DefaultProperties";
import Fields from "../fields";
import BaseComponent from "./BaseComponent";
import TextBox from "../TextBox";

/**
 * Объект - фигура, линия
 */
export default class LineComponent extends BaseComponent {
    static meta = {
        properties: Object.assign({}, defaultProperties, {
            scale: null,
            x1: {
                title: "X1",
                type: Fields.Field,
                defaultValue: 0
            },
            y1: {
                title: "Y1",
                type: Fields.Field,
                defaultValue: 0
            },
            x2: {
                title: "X2",
                type: Fields.Field,
                defaultValue: 200
            },
            y2: {
                title: "Y2",
                type: Fields.Field,
                defaultValue: 0
            },
            angle: null,
            strokeWidth: {
                title: "Ширина обводки",
                type: Fields.RangeField,
                defaultValue: 5,
                maxValue: 100,
                minValue: 1
            },
            lineHeight: {
                title: "Ширина строки",
                type: Fields.RangeField,
                defaultValue: 5,
                maxValue: 100,
                minValue: 1
            }
        }),
        icon: "line",
        title: "Линия",
        name: "Line",
        isShape: true
    };
    shouldComponentUpdate(n) {
        if (super.shouldComponentUpdate(n)) {
            return true;
        }
        const p = this.props;

        return p.isAlarm !== n.isAlarm;
    }

    /**
     * Рисуем фигуру в голове стрелки
     * @param {*} classNames список общих классов для стрелки
     * @param {*} source источник
     */
    renderBeforeLine(classNames, source) { }

    /**
     * Рисуем фигуру в хвосте стрелки
     * @param {*} classNames список общих классов для стрелки
     * @param {*} source источник
     */
    renderAfterLine(classNames, source) { }

    render() {
        const object = this.props.object;

        // Вычисляем угол стрелки относительно хвоста
        const angle = Math.atan2(object.y2 - object.y1, object.x2 - object.x1);
        // Вычисляем смещение линии с хвоста, чтобы не перекрывать рамку
        const lineOffsetX = -Math.cos(angle) * this.props.object.strokeWidth / 2;
        const lineOffsetY = -Math.sin(angle) * this.props.object.strokeWidth / 2;

        let fill = this.props.object.fill;
        let stroke = this.props.object.stroke;
        let opacity = this.props.object.opacity / 100;
        let source, cursor;
        const classNames = [];

        // Если данный объект является дочерним другому объекту, то получаем источника, и задаем стили
        if (this.props.object.parentObject) {
            source = this.props.object.parentObject.source;
            classNames.push("observ-area");
            fill = "#448866";
            stroke = null;
            cursor = this.props.isEditor ? "default" : "crosshair";
        }

        // Добавляем анимацию "мигание", если объект находится в тревоге
        if (this.props.isAlarm) {
            classNames.push("alarm-animation");
        }
        const titleFontSize = this.props.object.fontSize;
        const minX = Math.min(object.x1, object.x2);
        const minY = Math.min(object.y1, object.y2);
        const maxX = Math.max(object.x1, object.x2);
        const maxY = Math.max(object.y1, object.y2);

        // Вычисляем размеры прямоугольной занимаемой области стрелки, для установки плашки с текстом и кнопок
        const width = maxX - minX;
        const height = maxY - minY;
        return (
            <g
                style={{ cursor }}
                fill={fill}
                transform={`translate(${this.props.object.x} ${
                    this.props.object.y
                })`}
                ref={this.props.object.ref}
                className="object-transition"
            >
                <g
                    style={{ opacity }}
                    onClick={e =>
                        this.onAction({
                            object: this.props.object.source,
                            name: "plans"
                        })
                    }
                    stroke={stroke}
                >
                    <path
                        className={classNames.join(" ")}
                        data-object={source}
                        d={`M${this.props.object.x1},${this.props.object.y1} L${this.props.object.x2},${this.props.object.y2}`}
                        strokeWidth={this.props.object.lineHeight + this.props.object.strokeWidth }
                        
                    />
                    {this.renderBeforeLine(classNames, source)}
                    <path
                        className={classNames.join(" ")}
                        data-object={source}
                        transform={`translate(${lineOffsetX} ${lineOffsetY})`}
                        d={`M${this.props.object.x1 - lineOffsetX * 2},${this.props.object.y1 - lineOffsetY * 2} L${this.props.object.x2},${this.props.object.y2}`}
                        strokeWidth={this.props.object.lineHeight}
                        stroke={fill}
                    />
                    {this.renderAfterLine(classNames, source)}
                </g>
                {this.props.object.showText &&
                    this.props.object.sourceObject.text &&
                    this.props.object.sourceObject.text.length > 0 && (
                        <g
                            stroke="white"
                            style={{
                                transform: `translate(${width / 2 -
                                    175}px, ${height}px)`,
                                pointerEvents: "none"
                            }}
                        >
                            <TextBox
                                width="400"
                                x="40"
                                y="0"
                                fontSize={titleFontSize}
                            >
                                {this.props.object.sourceObject.text}
                            </TextBox>
                        </g>
                    )}
            </g>
        );
    }
}
