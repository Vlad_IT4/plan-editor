import React from "react";
import defaultProperties from "../DefaultProperties";
import Fields from "../fields";
import BaseComponent from "./BaseComponent";
import TextBox from "../TextBox";
import { SvgManager } from "../../Helpers.js";

/**
 * Объект - веб страница, при клике на этот объект, в окне камер должет появится браузер с открытой указанной страницей
 */
export default class WebPageComponent extends BaseComponent {
    static meta = {
        properties: Object.assign({}, defaultProperties, {
            source: {
                title: "Веб-страница",
                type: Fields.ListField,
                parcel: "webpages",
                hideElementIfNone: true
            },
            fill: null,
            stroke: null,
            angle: null,
            action: null
        }),
        icon: "webpage",
        title: "Веб-страница",
        name: "WebPage",
        isPreventScroll: true // Запрещаем скролл
    };

    render() {
        const titleFontSize = '' + this.props.object.fontSize;
        return (
            <g
                transform={`translate(${this.props.object.x} ${
                    this.props.object.y
                })`}
                className="object-transition"
            >
                <g
                    transform={`scale(${this.props.object.scale})`}
                    style={{ opacity: this.props.object.opacity / 100 }}
                >
                    {/* Показываем иконку */}
                    <use
                        xlinkHref={SvgManager.get("webpage")}
                        ref={this.props.object.ref}
                        onClick={e =>
                            this.onAction({
                                object: this.props.object.source,
                                name: "webpage:open"
                            })
                        }
                    />
                </g>
                {/* Показываем текст */}
                {this.props.object.showText &&
                    this.props.object.sourceObject.text &&
                    this.props.object.sourceObject.text.length > 0 && (
                        <g
                            stroke="white"
                            style={{
                                transform: `translate(${(64 *
                                    this.props.object.scale) /
                                    2 -
                                    175}px, ${80 * this.props.object.scale}px)`,
                                pointerEvents: "none"
                            }}
                        >
                            <TextBox
                                width="400"
                                x="40"
                                y="0"
                                fontSize={titleFontSize}
                            >
                                {this.props.object.sourceObject.text}
                            </TextBox>
                        </g>
                    )}
            </g>
        );
    }
}
