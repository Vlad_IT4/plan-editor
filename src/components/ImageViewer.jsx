import React, { Component } from "react";
import PropTypes from "prop-types";

import { truncateStr } from "../Helpers.js";

/**
 * Просмоторщик картинок.
 */
export default class ImageViewer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            imageWidth: 0,
            imageHeight: 0,
            imageSize: 0
        };
    }

    onLoadImage(ev) {
        const imgEl = ev.nativeEvent.target;
        const imageWidth = imgEl.naturalWidth;
        const imageHeight = imgEl.naturalHeight;
        const imageSize = Math.round((this.props.image.content.length * 6) / 8);

        this.setState({
            imageWidth,
            imageHeight,
            imageSize
        });
    }

    render() {
        return (
            <div className="image-viewer">
                <div className="image-viewer__preview">
                    <img
                        src={this.props.image.content}
                        alt={this.props.image.name}
                        title={this.props.image.name}
                        onLoad={this.onLoadImage.bind(this)}
                    />
                </div>
                <div className="image-viewer__info">
                    <h2
                        className="image-viewer__name"
                        title={this.props.image.name}
                    >
                        {truncateStr(this.props.image.name)}
                    </h2>
                    <div className="image-viewer__props">
                        <p>
                            <label>Размеры: </label>
                            <span>
                                {this.state.imageWidth}x{this.state.imageHeight}
                            </span>
                        </p>
                        <p>
                            <label>Размер: </label>
                            <span>
                                {Math.round(this.state.imageSize / 1024)} КБ
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}

PropTypes.propTypes = {
    image: PropTypes.object
};
