import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { SvgManager } from "../Helpers.js";

/**
 * Кнопка навигации списка объектов. Активируется, если объекты не вмещаются в видимую область меню
 */
class NavListButtonComponent extends PureComponent {
    render() {
        return (
            <div
                className={`nav-components__button nav-components__button_${
                    this.props.prefix
                }`}
                onClick={this.props.onClick}
            >
                <svg width="10" height="10" viewBox="0 0 64 64" fill="white">
                    <use xlinkHref={SvgManager.get("arrow")} />
                </svg>
            </div>
        );
    }
}

NavListButtonComponent.propTypes = {
    prefix: PropTypes.string,
    onClick: PropTypes.func
};

/**
 * Список всех доступных объектов. Выводится в шапке
 */
export default class ComponentList extends PureComponent {
    constructor(props) {
        super(props);
        this.listRef = React.createRef();
        this.containerRef = React.createRef();

        this.state = {
            showContols: false,
            scrollX: 0
        };
    }

    /**
     * Начало переноса объекта мышкой. В dataTransfer сохраняются параметры:
     * component - перетаскиваемый объект
     * parent - родитель, указывается в null, т.к. при перетаскивании из верхнего меню, у объекта не будет родителя
     */
    onDragStart = (ev, compName) => {
        const event = ev.nativeEvent;
        event.dataTransfer.dropEffect = "move";
        event.dataTransfer.setData("component", compName);
        event.dataTransfer.setData("parent", null);
    };

    /**
     * Отображаем объект в списке объектов
     * @param {Object} compName отображаемое имя объекта
     * @param {Number} ind индекс объекта в списке объектов Objects
     */
    renderComponent(compName, ind) {
        const comp = this.props.components[compName];

        if (comp && comp.meta.title) {
            return (
                <div
                    className="components__item"
                    draggable="true"
                    onDragStart={ev => this.onDragStart(ev, compName)}
                    key={ind}
                    onClick={e => this.props.onCreate(comp)}
                    style={{ cursor: "pointer" }}
                >
                    <div
                        className="components__image"
                        style={{ cursor: "pointer" }}
                    >
                        <svg width="32px" height="32px" viewBox="0 0 64 64">
                            <use xlinkHref={SvgManager.get(comp.meta.icon)} />
                        </svg>
                    </div>
                    <div
                        className="components__name"
                        style={{ cursor: "pointer" }}
                    >
                        <span>{comp.meta.title}</span>
                    </div>
                </div>
            );
        }
    }

    /**
     * Считаем, помещаются ли объекты в меню. Если нет, то активируем навигацию
     */
    calcSize = () => {
        if (this.containerRef.clientWidth < this.listRef.clientWidth) {
            this.setState({
                showContols: true
            });
        } else {
            this.setState({
                showContols: false
            });
        }
    };

    /**
     * Скролл блока компонентов
     */
    moveHandler(pos) {
        if (this.listRef) {
            this.setState({
                scrollX: Math.min(
                    Math.max(0, this.state.scrollX + pos * 100),
                    this.listRef.clientWidth - this.containerRef.clientWidth
                )
            });
        }
    }

    componentDidMount() {
        this.calcSize();
        window.addEventListener("resize", this.calcSize);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.calcSize);
    }

    render() {
        return (
            <div
                className="components__outer"
                ref={ref => (this.containerRef = ref)}
            >
                {this.state.showContols && (
                    <NavListButtonComponent
                        prefix="prev"
                        onClick={this.moveHandler.bind(this, -1)}
                    />
                )}
                <div
                    className="components__list"
                    ref={ref => (this.listRef = ref)}
                    style={{ marginLeft: -this.state.scrollX }}
                >
                    {Object.keys(this.props.components).map(
                        this.renderComponent.bind(this)
                    )}
                </div>
                {this.state.showContols && (
                    <NavListButtonComponent
                        prefix="next"
                        onClick={this.moveHandler.bind(this, 1)}
                    />
                )}
            </div>
        );
    }
}

ComponentList.propTypes = {
    props: PropTypes.object,
    onCreate: PropTypes.func
};
