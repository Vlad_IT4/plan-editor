export function truncateStr(str, count = 15, end = "...") {
    return str.length > count ? str.substr(0, count - 1) + end : str;
}
let spritePath = "";

/**
 * Класс для получение пути к спрайту по названию иконки
 */
export class SvgManager {
    /**
     * Установить путь к спрайту с иконками
     * @param {string} newSpritePath новый путь к иконкам
     */
    static setSpritePath(newSpritePath) {
        if (typeof newSpritePath === "string") {
            spritePath = newSpritePath;
        }
    }

    /**
     * Получить путь к иконке
     * @param {string} name название иконки
     */
    static get(name) {
        return `${spritePath}#${name}`;
    }
}

/**
 * Приведение полярных координат в декартовы
 */
export function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
    var angleInRadians = ((angleInDegrees - 90) * Math.PI) / 180.0;
    return {
        x: centerX + radius * Math.cos(angleInRadians),
        y: centerY + radius * Math.sin(angleInRadians)
    };
}

/**
 * Функция создания строки пути для тега path, которая будет отображать видимую область камеры
 */
export function describeArc(x, y, radius, startAngle, endAngle) {
    startAngle = Math.max(startAngle, -89.995);
    endAngle = Math.min(endAngle, 269.995);
    var start = polarToCartesian(x, y, radius, endAngle);
    var end = polarToCartesian(x, y, radius, startAngle);

    var largeArcFlag = endAngle - startAngle < 180 ? "0" : "1";

    var d = [
        "M",
        start.x,
        start.y,
        "A",
        radius,
        radius,
        0,
        largeArcFlag,
        0,
        end.x,
        end.y
    ].join(" ");

    return d;
}

export function debounce(fn, time) {
    let timeout;

    return function(...args) {
        const functionCall = () => fn.apply(this, args);

        clearTimeout(timeout);
        timeout = setTimeout(functionCall, time);
    };
}

function getRatioBySize(width, height, newWidth, newHeight) {
    let ratio;
    if (newWidth > newHeight) {
        ratio = newWidth / width;
    } else {
        ratio = newHeight / height;
    }
    return ratio;
}

/**
 * Пропорциональное изменение размеров картинки
 * @param {string} base64Content картинка
 * @param {string} mimeType тип картинки
 * @param {number} newWidth новая ширина картинки
 * @param {number} newHeight новая высота картинки
 */
export function resizeImage(base64Content, mimeType, newWidth, newHeight) {
    return new Promise((resolve, reject) => {
        const image = new Image();

        image.onload = () => {
            let ratio = getRatioBySize(
                image.width,
                image.height,
                newWidth,
                newHeight
            );
            // Если картинка и так маленькая, ничего не ресайзим
            if (ratio >= 1) {
                resolve(base64Content);
            }
            const canvas = document.createElement("canvas");

            canvas.width = image.width * ratio;
            canvas.height = image.height * ratio;

            const context = canvas.getContext("2d");

            context.drawImage(
                image,
                0,
                0,
                image.width * ratio,
                image.height * ratio
            );

            let newBase64Content;

            if (["image/jpeg", "image/webp"].includes(mimeType)) {
                newBase64Content = canvas.toDataURL("image/webp", 0.8);
            } else {
                newBase64Content = canvas.toDataURL(mimeType);
            }

            // на всякий случай, если картинка после обработку получилась объемнее, используем исходную картинку.
            if (newBase64Content.length < base64Content.length) {
                resolve(newBase64Content);
            } else {
                resolve(base64Content);
            }
        };
        image.src = base64Content;
    });
}
