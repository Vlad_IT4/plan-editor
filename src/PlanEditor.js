import React from 'react';
import PropTypes from 'prop-types';
import Objects from './components/objects/objects.js';

// Components
import BaseComponent from './components/objects/BaseComponent';
import Transformator from './components/Transformator';
import PropertiesEditor from './components/PropertiesEditor';
import ComponentList from './components/ComponentList';
import ViewerPanel from './components/ViewerPanel';
import SvgIcons from './components/SvgIcons';
import ImageStorage from './storage/ImageStorage';
import PlanBackground from './PlanBackground';
import Controls from './components/Controls';

// Libs
import * as Helper from './Helpers.js';
import Memento from './memento/Memento.js';
import BufferManager from './BufferManager.js';
import CompassPlanStorage from './storage/CompassPlanStorage';
import ObjectsManager from './ObjectsManager';
import ObjectsTransformator from './ObjectsTransformator';
// Styles
import './PlanEditor.css';

class PlanEditor extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			canvas: {
				width: 1000,
				height: 600,
				backgroundColor: "#1a1a1a",
				backgroundImage: null,
				backgroundGridColor: "#646464",
				component: Objects.CanvasPlan,
				dateCreate: new Date(),
				dateChanges: new Date(),
				id: -1,
				shortcut: 0,
			},
			canvasIteration: 0,
			selectedComponentIndex: -1,
			startParams: null,
			zoom: 0,
			scrollX: 0,
			scrollY: 0,
			isObjectEditor: false,
			transferringObject: null,
			alarms: [],
			objectsIterations: []
		}
		this.objectsManager = new ObjectsManager(this);
		this.objectsTransformator = new ObjectsTransformator(this);
		this.isMoving = false;
		this.oldSelectedComponentIndex = null;
		this.dragging = false;
		this.svgCanvas = React.createRef();
		this.container = React.createRef();
		this.propsParcel = {};
		this.id = Math.floor(Math.random() * 10000000000000).toString(36);
		this._canvasBackgroundImage = null;
		this._storage = new CompassPlanStorage();

		this.propsParcel['streams'] = props.getSources;
		this.propsParcel['macroses'] = props.getMacroses;
		this.propsParcel['webpages'] = props.getWebPages;
		this.propsParcel['tours'] = props.getTours;
		this.propsParcel['plans'] = props.getPlans;
		this.propsParcel['salvos'] = props.getSalvos;

		this.propsParcel['backgroundSize'] = [
			{ text: 'Растянуть', value: '' },
			{ text: 'Уместить', value: 'meet' },
			{ text: 'Исходный размер', value: 'original' }
		];

		// Матрицы цветовых фильтров. https://developer.mozilla.org/en-US/docs/Web/SVG/Element/feColorMatrix
		this.propsParcel['imageFilters'] = [
			{ text: 'Инвертированный', value: '-1 0 0 0 1  0 -1 0 0 1  0 0 -1 0 1 0 0 0 1 0' },
			{ text: 'Красный', value: '1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0' },
			{ text: 'Синий', value: '0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 1 0' },
			{ text: 'Зеленый', value: '0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1 0 ' },
			{ text: 'Желтый', value: '1 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1 0' },
			{ text: 'Пурпурный', value: '1 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 1 0' },
			{ text: 'Циановый', value: '0 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0' },
			{ text: 'Темный', value: '.5 0 0 0 0 0  .5 0 0 0 0 0  .5 0 0 0 0 0 1 0' },
			{ text: 'Светлый', value: '1.5 0 0 0 0 0 1.5 0 0 0 0 0 1.5 0 0 0 0 0 1 0' },
			{ text: 'Черно-белый светлый', value: '1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 0 0 0 1 0' },
			{ text: 'Черно-белый средний', value: '0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 0 0 1 0' },
			{ text: 'Черно-белый темный', value: '0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 0 1 0' },
		];

		this.memento = new Memento();
		this.memento.updateObject = object => {
			if (!object) {
				return;
			}
			const objectId = object.id;
			const objectUpdater = this.objectsManager.getObjectById(objectId);
			if (!objectUpdater) {
				return;
			}
			Object.assign(objectUpdater, object);

			if (object.id === -1) {
				objectUpdater.id = -1;
				this.setState({
					canvas: objectUpdater,
					canvasIteration: this.state.canvasIteration + 1
				});
			} else {
				this.updateIterationObject(objectUpdater);
			}
		}
		this.memento.reviveObject = (object, pos) => {
			if (this.objectsManager.getObjectById(object.id)) {
				return;
			}

			if (object.parent !== null && object.parent >= 0) {
				const parent = this.objectsManager.getObjectById(object.parent);
				if (parent) {
					object.parentObject = parent;
					parent.childs.push(object);
				}
			}

			let objects = this.objectsManager.objects;
			objects.splice(pos, 0, object);

			var objectsIterations = this.state.objectsIterations;
			objectsIterations.splice(pos, 0, 0);

			if (Array.isArray(object.childs)) {
				object.childs.forEach(child => {
					objects.push(child);
					objectsIterations.push(0);
				});
			}

			this.setState({
				objectsIterations
			}, () => {
				this.updateAllComponents();
				this.oldSelectedComponentIndex = this.state.selectedComponentIndex;
			});
		}
		this.memento.deadObject = (object, pos) => {
			this.deleteObjectAndChilds(object, pos);
		}
		this.memento.transferZObject = (objectId, from, to) => {
			this.objectsManager.moveZObjectTo(this.objectsManager.getObjectById(objectId), from, to, true);
		}
		this.memento.changeSelectIndex = (newIndex) => {
			if (newIndex >= -1) {
				this.setSelectIndexObject(newIndex);
			}
		}
		this.memento.rememberObject(this.state.canvas);
		this.bufferManager = new BufferManager(this);
		this.lastMousePos = null;
		// Устанавливаем путь до изображения со спрайтами
		Helper.SvgManager.setSpritePath(props.svgSpritePath || 'sprites.svg');
	}

	/**
	 * Обновляем итерацию объекта и его родителя
	 */
	updateIterationObject(object) {
		const index = this.objectsManager.getObjectIndex(object);	
		if (index === -1) {
			return;
		}

		const objectsIterations = this.state.objectsIterations;
		objectsIterations[index]++;
		this.setState({objectsIterations: objectsIterations});

		// Обновляем родителя, если он есть
		if (object.parentObject) {
			this.updateIterationObject(object.parentObject);
		}
	}

	componentDidMount() {
		document.addEventListener('mouseup', this.handleMouseUp);
		this.setSelectIndexObject(-1);
		setTimeout(() => {
			this.onZoomFill();
		}, 4);
	}

	componentWillUnmount() {
		document.removeEventListener('mouseup', this.handleMouseUp);
		this.bufferManager.destroy();
        if (typeof this.props.onDestroy === 'function') {
			this.props.onDestroy();
		}
	}

	handleMouseUp = (event) => {
		this.setState({ transferringObject: null });
		this.endDrag();
	}

	handleKeyPress = (event) => {
		const keyCode = event.keyCode;
		const isCtrl = event.ctrlKey;

		const object = this.state.transformatorObject;
		// Манипуляция над объектом
		if (object) {
			// Перемещение стрелками
			if (keyCode >= 37 && keyCode <= 40) {
				this.objectsManager.movingObject(object, 41 - keyCode);
			} else if (keyCode === 46) {
				this.deleteObject(object);
			}
		}
		// With ctrl
		if (isCtrl) {
			switch(keyCode) {
				case 89:
					this.memento.redo();
				break;
				case 90:
					this.memento.undo();
				break;
				default:
				break;
			}
		}
	}

	/**
	 * Получаем координаты холста относительно документа, и его ширину
	 */
	getOffset() {
		let divPos = this.svgCanvas.current.getBoundingClientRect();

		return {
			x: divPos.left,
			y: divPos.top,
			width: this.state.canvas.width,
			height: this.state.canvas.height
		};
	}

	/**
	 * Получаем координаты мыши относительно холста, с учетом зума
	 * @param {Event} event
	 * @param {Number} zoom
	 */
	getMouseCoords(event, zoom = this.state.zoom) {
		let offset = this.getOffset();

		return {
			x: (event.clientX - offset.x) / zoom,
			y: (event.clientY - offset.y) / zoom
		}
	}

	/**
	 * Показываем трансформирующий инструмент для указанного компонента
	 * @param {Number} selectedComponentIndex
	 */
	showTransformator(selectedComponentIndex) {
		let object = this.objectsManager.getObjectByIndex(selectedComponentIndex);
		if (!object) {
			object = this.state.canvas;
		}
		this.memento.rememberObject(object);
		this.setState({
			transformatorObject: object
		});
	}

	setSelectIndexObject(newIndex) {
		this.setState({ selectedComponentIndex: newIndex }, () => {
			this.showTransformator(newIndex);
		});
	}
	/**
	 * Изменяем index выбранного компонента, и показываем для него трансформирующий инструмент
	 * @param {Number} newIndex
	 */
	changeSelectIndexComponent(newIndex) {
		if (newIndex === this.state.selectedComponentIndex) {
			return;
		}
		this.memento.selectObject(this.oldSelectedComponentIndex, newIndex);
		this.oldSelectedComponentIndex = newIndex;
		this.setSelectIndexObject(newIndex);
	}

	/**
	 * Начинаем drag&drop на компонент, выполняя действие из mode
	 * @param {Number} ind - номер компонента
	 * @param {Enum} mode {rotate, scale, scroll}
	 * @param {Event} event
	 */
	startDrag = (ind, mode, event) => {
		event.stopPropagation();

		if (ind !== null && ind >= 0) {
			const object = this.objectsManager.getObjectByIndex(ind);
			this.memento.rememberObject(object);
			let mousePos = this.getMouseCoords(event);

			this.setState({
				startParams: {
					object: {
						angle: object.angle,
						scale: object.scale,
						x: object.x,
						y: object.y,
						width: object.width,
						height: object.height
					}, mousePos, offset: {
						x: object.x - mousePos.x,
						y: object.y - mousePos.y
					}
				}
			});
			this.dragging = true;
			this.modeDrag = mode;
			this.changeSelectIndexComponent(ind);
		}
	}

	/**
	 * Скролл холста за позицией мыши
	 */
	canvasScroll(mouseFixPos) {
		const newScrollX = mouseFixPos.x + this.state.startParams.offsetScrollX;
		const newScrollY = mouseFixPos.y + this.state.startParams.offsetScrollY;

		this.setState({
			scrollX: newScrollX,
			scrollY: newScrollY
		});
	}

	getTransformatorObject() {
		if (this.state.transformatorObject) {
			return this.state.transformatorObject;
		}
		return false;
	}
	/**
	 * Метод, который выполняется при движении мыши, когда выбран компонент и включен режим this.dragging
	 * @param {Event} event
	 */
	drag = (event) => {
		if (this.state.startParams === null || !this.dragging) {
			return;
		}

		const mousePos = this.getMouseCoords(event);
		const mode = this.modeDrag;
		const object = this.objectsManager.getObjectByIndex(this.state.selectedComponentIndex);

		if (mode === 'scroll') {
			const mouseFixPos = this.getMouseCoords(event, 1);
			this.canvasScroll(mouseFixPos);
		}

		if (!object || object.locked) {
			return;
		}
		if (!this.props.isEditor && 
			(
				(this.state.transferringObject && 
				this.state.transferringObject.source) || 
				this.objectsManager.isObjectScrollBroke(this.state.transferringObject) || 
				this.state.transferringObject.component.meta.isPreventDrag
			)
		) {
			return;
		}

		if (mode === 'rotate') {
			this.objectsTransformator.objectRotate(object, mousePos);
		} else if (mode === 'scale') {
			this.objectsTransformator.objectScale(object, mousePos);
		} else if (mode.startsWith('vector_')) {
			this.objectsTransformator.objectVectorMove(object, mousePos, mode);
		} else if (mode.startsWith('resize_')) {
			this.objectsTransformator.objectResize(object, mousePos, mode, event.shiftKey);
		} else if (mode === 'move') {
			this.objectsTransformator.objectMove(object, mousePos);
		}
		this.updateIterationObject(object);
	}

	/**
	 * При отпускании мыши отключаем dragging и обнуляем режим
	 */
	endDrag = () => {
		this.dragging = false;
		this.modeDrag = null;
		this.onChange();
	}

	/**
	 * Скролл холста
	 * @param {Event} event
	 */
	scrollDrag(event) {
		if ((!this.props.isEditor && this.state.transferringObject && this.state.transferringObject.source) || this.objectsManager.isObjectScrollBroke(this.state.transferringObject)) {
			return;
		}
		this.modeDrag = 'scroll';
		this.isMoving = false;
		let mousePos = this.getMouseCoords(event, 1);
		this.setState({
			startParams: {
				offsetScrollX: this.state.scrollX - mousePos.x,
				offsetScrollY: this.state.scrollY - mousePos.y
			}
		})
		this.dragging = true;
	}

	/**
	 * Детектим клик по пустой области холста для того, чтобы убрать выделение с объектов
	 */
	onCanvasClick(event) {
		if (event.button === 0) {
			this.changeSelectIndexComponent(-1);
		}
		const lastMousePos = this.getMouseCoords(event);
		lastMousePos.x = lastMousePos.x - this.state.scrollX / this.state.zoom;
		lastMousePos.y = lastMousePos.y - this.state.scrollY / this.state.zoom;
		this.lastMousePos = lastMousePos;
	}

	/**
	 * Зум холста, с центровкой в указанную точку
	 * @param {Number} zoom величина увеличения
	 * @param {Number} centerX x центра фокусировки увеличения
	 * @param {Number} centerY y центра фокусировки увеличения
	 */
	zoomTo(zoom, centerX, centerY) {
		const oldZoom = this.state.zoom;
		const moreWidth = this.state.canvas.width * zoom - this.state.canvas.width * oldZoom;
		const moreHeight = this.state.canvas.height * zoom - this.state.canvas.height * oldZoom;

		const newScrollX = -moreWidth / (this.state.canvas.width / centerX);
		const newScrollY = -moreHeight / (this.state.canvas.height / centerY);
		if (zoom > 0.05) {
			this.setState({
				scrollX: this.state.scrollX + newScrollX,
				scrollY: this.state.scrollY + newScrollY,
				zoom: Math.max(0.01, zoom)
			});
		}
	}

	/**
	 * Зум холста до исходного размера с центровкой посередине
	 */
	onZoomToSource = () => {
		const canvas = this.svgCanvas.current;
		if (!canvas) {
			return;
		}
		let width = canvas.clientWidth;
		let height = canvas.clientHeight;
		let canvWidth = this.state.canvas.width;
		let canvHeight = this.state.canvas.height;

		this.setState({
			scrollX: Math.max(0, (width / 2 - canvWidth / 2)),
			scrollY: Math.max(0, (height / 2 - canvHeight / 2)),
			zoom: 1
		});
	}

	/**
	 * Зум холста до заполнения всего окна
	 */
	onZoomFill = () => {
		const canvas = this.svgCanvas.current;
		if (!canvas) {
			return;
		}
		let width = canvas.clientWidth;
		let height = canvas.clientHeight;
		let canvWidth = this.state.canvas.width;
		let canvHeight = this.state.canvas.height;
		let zoom = 1;

		if (canvWidth / width < canvHeight / height) {
			zoom = height / canvHeight;
		} else {
			zoom = width / canvWidth;
		}
		this.setState({
			scrollX: Math.max(0, (width / 2 - canvWidth / 2 * zoom)),
			scrollY: Math.max(0, (height / 2 - canvHeight / 2 * zoom)),
			zoom
		});
	}

	/**
	 * Обрабатываем колесико мыши, высчитывая центральную точку для зума
	 * @param {Event} e
	 */
	onMouseWheel = (e) => {
		e.preventDefault();
		let delta = -e.deltaY;

		const zoom = this.state.zoom + delta / 5000;
		const mousePos = this.getMouseCoords(e, this.state.zoom);

		const centerX = mousePos.x - this.state.scrollX / this.state.zoom;
		const centerY = mousePos.y - this.state.scrollY / this.state.zoom;

		this.zoomTo(zoom, centerX, centerY);
	}

	/**
	 * Добавление увеличения на указанную кратность
	 * @param {Number} add число увеличения кратности зума
	 */
	addZoom = (add) => {
		const container = this.svgCanvas.current;
		const centerX = (container.clientWidth / 2 - this.state.scrollX) / this.state.zoom;
		const centerY = (container.clientHeight / 2 - this.state.scrollY) / this.state.zoom;
		this.zoomTo(this.state.zoom + add, centerX, centerY);
	}

	/**
	 * Событие изменения свойства объекта
	 * @param {Number} objId номер объекта
	 * @param {String} key название свойства
	 * @param {String} value значение свойства
	 */
	onPropertyChange = (objId, key, value) => {
		const objects = this.objectsManager.objects;
		if (objId !== null && objId >= 0) {
			objects[objId][key] = value;
		} else {
            const canvas = this.state.canvas;
            canvas[key] = value;
			this.setState({
				canvas,
				canvasIteration: this.state.canvasIteration+1
			});
		}

		this.updateIterationObject(objects[objId]);
	}

	onPropertyChangeEnd = (objId) => {
		let obj;
		if (objId === -1) {
			obj = this.state.canvas;
		} else {
			obj = this.objectsManager.getObjectByIndex(objId);
		}
		this.memento.takeSnapshot(obj, this.oldSelectedComponentIndex, this.state.selectedComponentIndex);
		this.onChange();
	}
	/**
	 * Получение координат холста по центру контейнера
	 */
	getCenterCanvas() {
		const canvas = this.svgCanvas.current;
		if (!canvas) {
			return;
		}
		const canvasWidth = canvas.clientWidth / this.state.zoom;
		const canvasHeight = canvas.clientHeight / this.state.zoom;

		const x = canvasWidth / 2 - this.state.scrollX / this.state.zoom;
		const y = canvasHeight / 2 - this.state.scrollY / this.state.zoom;
		return { x, y };
	}
	isSelectObject() {
		return this.state.selectedComponentIndex !== null && this.state.selectedComponentIndex >= 0;
	}
	
	/**
	 * Метод добавления объекта на форму
	 * @param {Object} comp
	 */
	createComponent = (component, x, y, parent) => {
		if (this.state.isObjectEditor) {
			return;
		}

		if (!isFinite(x) || !isFinite(y)) {
			const coords = this.getCenterCanvas();
			x = coords.x;
			y = coords.y;
		}

		let objects = this.objectsManager.objects;

		let newObject = {
			x, y,
			ref: React.createRef(),
			component: component,
			locked: false,
			id: this.objectsManager.count, // назначаем индекс равный кол-во объектов
			parent: parent && parent.component ? parent.id : null
		}

		if (parent) {
			if (!Array.isArray(parent.childs)) {
				parent.childs = [];
			}
			newObject.parentObject = parent;
			parent.childs.push(newObject);
			this.updateIterationObject(parent);
		}
		for (let name in component.meta.properties) {
			const prop = component.meta.properties[name];

			if (prop && 'defaultValue' in prop) {
				newObject[name] = prop.defaultValue;
			}
		}

		objects.push(newObject);

		var objectsIterations = this.state.objectsIterations.slice();
		objectsIterations.push(0);

		newObject.isEditing = !!component.meta.editor;

		this.setState({
			objectsIterations,
			isObjectEditor: newObject.isEditing
		}, () => {
			this.oldSelectedComponentIndex = this.state.selectedComponentIndex;
			this.memento.addObject(objects.indexOf(newObject), newObject, this.oldSelectedComponentIndex, objects.indexOf(newObject));
			this.setSelectIndexObject(objects.indexOf(newObject));
		});
		return newObject;
	}

	onDrop = (ev) => {
		const compName = ev.nativeEvent.dataTransfer.getData('component');
		const comp = Objects[compName];

		if (comp) {
			const parentId = ev.nativeEvent.dataTransfer.getData('parent');
			const parent = this.objectsManager.getObjectById(parentId);

			const x = ev.nativeEvent.offsetX - 32 - this.state.scrollX;
			const y = ev.nativeEvent.offsetY - 32 - this.state.scrollY;
			this.createComponent(comp, x / this.state.zoom, y / this.state.zoom, parent);
		}
	}

	/**
	 * Выделяем объект на холсте
	 */
	selectObject = (obj) => {
		const newSelectedIndex = this.objectsManager.getObjectIndex(obj);

		this.changeSelectIndexComponent(newSelectedIndex);
	}

	onDragOver = (ev) => {
		ev.preventDefault();
	}

	/**
	 * Метод сохранения плана
	 */
	savePlan = () => {
		const plan = {
			canvas: this.state.canvas,
			objects: this.objectsManager.objects,
			dateChanges: new Date()
		};

		// Удаляем все ref ссылки из JSON, а также вместо класса компонента, используем его имя
		const data = JSON.stringify(plan, BaseComponent.prototype.jsonParseFilter);
		return {
			data,
			shortcut: this.state.canvas.shortcut,
			description: this.state.canvas.name,
		};
	}

	loadObjects(objects, saveMemento=true) {
		if (!Array.isArray(objects) || objects.length === 0) {
			return false;
		}
		objects.forEach((obj, ind) => {
			const compName = obj.component;
			if (!(compName in Objects)) {
				return;
			}
			obj.component = Objects[compName];
			obj.ref = React.createRef();
			obj.childs = objects.filter(child => {
				return child.parent === obj.id;
			});
			if (typeof obj.parent === 'number') {
				obj.parentObject = this.objectsManager.getObjectById(obj.parent, objects);
			}
			if (obj.image) {
				const img = ImageStorage.getById(obj.image);
				obj.image = img;
				this.updateIterationObject(obj);
			}
			this.objectsManager.add(obj);
		});

		const lastObject = this.objectsManager.getLastObject();
		const lastObjectIndex = this.objectsManager.getObjectIndex(lastObject);

		this.setState({
			objectsIterations: this.state.objectsIterations.concat(Array(objects.length).fill(0))
		}, () => {
			if (saveMemento) {
				this.memento.addObject(lastObjectIndex, lastObject, this.state.selectedComponentIndex, lastObjectIndex);
			}
			this.setSelectIndexObject(lastObjectIndex);
		});
	}
	/**
	 * Загружаем план
	 * @param {Event} ev
	 */
	openPlan = (planData) => {
		let plan, canvas;

		if (planData.data.trim().length === 0) {
			canvas = this.state.canvas;
			canvas.name = planData.description;
			canvas.shortcut = planData.shortcut;
		} else {
			try {
				plan = JSON.parse(planData.data);
			} catch (e) {
				alert('Ошибка открытия файла проекта. Файл некорректен');
				return;
			}

			this.loadObjects(plan.objects, false);
			canvas = plan.canvas;
			canvas.component = Objects['CanvasPlan'];
			canvas.id = -1;
			canvas.shortcut = planData.shortcut;
			canvas.name = planData.description;
		}

		this.setState({
			canvas: canvas,
			canvasIteration: 0
		}, () => {
			this.setSelectIndexObject(-1);
			let backImage = canvas.backgroundImage;
			if (backImage) {
				const img = ImageStorage.getById(backImage);
				let canvas = this.state.canvas;
				canvas.backgroundImage = img;
				this.setState({
					canvas
				});
			}
		});
	}

	/**
	 * Открываем план
	 * @param {String} file
	 */
	loadPlan(file) {
		var reader = new FileReader();
		reader.onload = this.openPlan;

		reader.readAsText(file);
	}

	/**
	 * Событие завершения работы редактора объекта
	 */
	onEditObjectEnd = (object) => {
		this.updateIterationObject(object);
		if (object.parentObject) {
			this.updateIterationObject(object.parentObject);
		}
		this.setState({ isObjectEditor: false });
	}

	/**
	 * Является ли выбранный объект последним в списке объектов
	 */
	isLastObject() {
		return this.objectsManager.count === 0 || this.objectsManager.getObjectIndex(this.state.transformatorObject) >= this.objectsManager.count - 1;
	}

	/**
	 * Является ли выбранный объект первым в списке объектов
	 */
	isFirstObject() {
		return this.objectsManager.count === 0 || this.objectsManager.getObjectIndex(this.state.transformatorObject) <= 0;
	}

	/**
	 * Выбираем объект для переноса 
	 */
	selectElementForDrag(object) {
		// eslint-disable-next-line
		this.state.transferringObject = object; // Меняем объект вручную, т.к. через setState он не сразу туда попадает
		this.setState({ transferringObject: object });
	}

	/**
	 * Начинаем перенос объекта. Устанавливаем изображение drag&drop и необходимые данные
	 */
	dragStartOuter(ev) {
		if (!((this.state.transferringObject && this.state.transferringObject.source)
				|| this.objectsManager.isObjectScrollBroke(this.state.transferringObject))) {
			return;
		}
		if (this.state.transferringObject.component.meta.isPreventDrag) {
			return;
		}
		ev.dataTransfer.setDragImage(this.state.transferringObject.ref.current, 0, 0);
		
		if (this.props.onDragStart === 'function') {
			this.props.onDragStart(ev, this.state.transferringObject);
		}
	}

	/**
	 * Завершаем drag&drop, сбрасываем данные
	 */
	onDragEnd = (ev) => {
		this.handleMouseUp(ev);
		if (typeof this.props.onDragEnd === 'function') {
			this.props.onDragEnd(ev, this.state.transferringObject);
		}
	}

	/**
	 * Ообновление всех объектов
	 */
	updateAllComponents = () => {
		this.setState({
			objectsIterations: this.state.objectsIterations.map(it => it+1)
		});
	}

	/**
	 * Получить объекты по значению поля
	 */
	getObjectsByFieldValue(field, value) {
		if (field === undefined || value === undefined) {
			return [];
		}
		const res = this.objects.filter(obj => {
			return obj[field] === value;
		});
		return res;
	}

	/**
	 * Установка режима объекта
	 */
	setObjectMode = (object, mode) => {
		this.setObjectProp(object, 'mode', mode);
	}

	/**
	 * Установка свойства объекта
	 */
	setObjectProp = (object, prop, value) => {
		object[prop] = value;
		this._plan.updateIterationObject(object);
	}

	/**
	 * Получение источника объекта по номеру
	 */
	getSourceObjectById(id, method) {
		if (typeof method !== 'function') {
			return null;
		}
		const sources = method({});
		for (const obj of sources) {
			if (obj.value.toString() === id.toString()) {
				return obj;
			}
		}
	}

	deleteObjectAndChilds(object, isRemoveFromParent=true) {
		const pos = this.objectsManager.getObjectIndex(object);

		if (pos === -1) {
			return;
		}

		if (Array.isArray(object.childs)) {
			object.childs.forEach(obj => this.deleteObjectAndChilds(obj, false), this);
		}

		const objects = this.objectsManager.objects;
		objects.splice(pos, 1);
		let objectsIterations = this.state.objectsIterations;
		objectsIterations.splice(pos, 1);

		this.setState({ objectsIterations, transformatorObject: null }, () => {
			this.updateAllComponents();
		});

		if (object.parentObject && isRemoveFromParent) {
			this.updateIterationObject(object.parentObject);

			const childIndex = object.parentObject.childs.indexOf(object);
			if (childIndex >= 0) {
				object.parentObject.childs.splice(childIndex, 1);
			}
		}
	}
	/**
	 * Удалям объект и всех его дочерних объектов
	 */
	deleteObject = (object) => {
		const pos = this.objectsManager.getObjectIndex(object);

		if (pos === -1) {
			return;
		}
		this.memento.deleteObject(pos, object, this.oldSelectedComponentIndex, this.state.selectedComponentIndex);
		this.deleteObjectAndChilds(object);
	}

	/**
	 * Выполняем действие объекта
	 */
	triggerAction(action) {
		if (this.props.onAction && !this.props.isEditor) {
			this.props.onAction(action);
		}
	}

	/**
	 * Детектим клик по наблюдаемой области объектов
	 */
	onClickObservArea = (e) => {
		if (this.props.isEditor) {
			return;
		}
		// Находим все элементы, которые смотрят на кликнутую точку
		const elements = document.elementsFromPoint(e.clientX, e.clientY);
		// Получаем только объекты, у которых установлен источник и имеется класс observ-area
		const observAreas = elements.filter(e => e.classList.contains('observ-area'));
		let sources = observAreas.map(o => o.dataset.object).filter(o => typeof o === 'string');
		// Удаляем повтоярющиеся элементы и отправляем в событие onClickObservArea
		if (typeof this.props.onClickObservArea === 'function') {
			this.props.onClickObservArea(Array.from(new Set(sources)));
		}
	}

	onLoadImageObject = (object) => {
		if (object.ref) {
			let width = object.ref.current.getBBox().width;
			let height = object.ref.current.getBBox().height;

			object.startWidth = width;
			object.startHeight = height;

			// Если размеры картинки выходят за пределы размеров холста, то пропорционально уменьшаем картинку до размеров холста
			if (width > this.state.canvas.width) {
				let ratio = width / height;
				width = this.state.canvas.width;
				height = width / ratio;
			} else if (height > this.state.canvas.height) {
				let ratio = height / width;
				height = this.state.canvas.height;
				width = height / ratio;
			}

			object.width = width;
			object.height = height;
			
			this.memento.takeSnapshot(object, this.oldSelectedComponentIndex, this.state.selectedComponentIndex);
			this.onChange();
			this.updateIterationObject(object);
		}
	}

	onChange() {
		if (this.props.isEditor && typeof this.props.onChange === 'function') {
			this.props.onChange();
		}
	}

	render() {
		let currentEditor;
		const getComponentsList = (list, isRoot=true, isAlarmParent=false) => {
			return list.map((obj) => {
				const ind = this.objectsManager.getObjectIndex(obj);

				const iteration = this.state.objectsIterations[ind];
				let isAlarm = isAlarmParent;

				if (!(obj && obj.component)) {
					return null;
				}

				if (obj.parentObject && isRoot) {
					return null;
				}
				 
				const Comp = obj.component;
				obj.sourceObject = {};

				if (obj.source) {
					let method = this.props.getPlans;
					let compName = Comp.meta.name;

					if (compName === 'WebPage') {
						method = this.props.getWebPages;
					} else if (compName === 'Button') {
						method = this.props.getMacroses;
					} else if (compName === 'Tour') {
						method = this.props.getTours;
					} else if (compName === 'Salvo') {
						method = this.props.getSalvos;
					} else if (['MicroPhone', 'Camera'].includes(compName)) {
						method = this.props.getSources;
					}

					obj.sourceObject = this.getSourceObjectById(obj.source, method) || {};
					isAlarm = this.state.alarms.includes(obj.source);
				}

				const Editor = Comp.meta.editor;
				if (Comp.create) {
					Comp.create();
				}
				if (Editor && this.state.isObjectEditor && obj.isEditing) {
					currentEditor = <Editor zoom={this.state.zoom} scroll={{ x: this.state.scrollX, y: this.state.scrollY }} canvWidth={this.state.canvas.width} canvHeight={this.state.canvas.height} onEditEnd={this.onEditObjectEnd} object={obj} />;
					return null;
				}
				let isRemovedSource = false;
				if (!this.props.isEditor) {
					const source = Comp.meta.properties.source;

					if (source.hideElementIfNone) {
						if (!('text' in obj.sourceObject) && !(obj.parentObject && 'text' in obj.parentObject.sourceObject)) {
							isRemovedSource = true;
						}
					}
				}

				const isHaveAction = Comp.meta.properties.action && obj.action.name;
				const isDraggable = !Comp.meta.isPreventDrag;

				return (
					<g key={ ind } style={ isRemovedSource ? ({ opacity: 0.5, cursor: 'no-drop' }) : ({}) }>
						<g style={{ opacity: 0.4 }} strokeWidth="1" stroke="#448866" fill="#448866">
							{ obj.childs && !obj.isEditing && getComponentsList(obj.childs, false, isAlarm)}
							{ ['MicroPhone', 'Camera'].includes(obj.component.meta.name) && (
								<g style={{ opacity: obj.opacity / 100 }} transform={`translate(${obj.x} ${obj.y})`} className="object-transition">
									<g transform={`rotate(${obj.angle} ${32 * obj.scale} ${32 * obj.scale})`}>
										<g transform={`scale(${obj.scale})`}>
											<path
											onMouseDown={e => e.preventDefault()}
											className={ `observ-area ${isAlarm && ' alarm-animation-stroke' }` }
											data-object={obj.source}
											fill="none"
											style={{ pointerEvents: this.props.isEditor && 'none', cursor: this.props.isEditor ? 'default' : (isRemovedSource ? 'no-drop' : 'crosshair') }}
											strokeWidth={obj.viewLength}
											d={
												obj.component.meta.name === 'Camera' ? 
													Helper.describeArc(32, 32, obj.viewDistance + obj.viewLength / 2 + 50, 90 - obj.viewRadius / 2, 90 + obj.viewRadius / 2)
													: Helper.describeArc(32, 32, -50 + obj.viewLength / 2 + 50, 90 - 359.99999 / 2, 90 + 359.99999 / 2)	
											}></path>
										</g>
									</g>
								</g>
							)}
							{ obj.childs && obj.isEditing && getComponentsList(obj.childs, false, isAlarm)}
						</g>
						<g onClick={!this.props.isEditor && isHaveAction ? (() => this.triggerAction(obj.action)) : undefined} draggable={isDraggable} onMouseUp={e => this.onPropertyChangeEnd(ind)} onMouseDown={this.props.isEditor ? this.startDrag.bind(this, ind, 'move') : this.selectElementForDrag.bind(this, this.objectsManager.getObjectByIndex(ind))} key={ind}>
							<Comp iteration={iteration} canvasId={this.id} isAlarm={isAlarm} object={obj} isEditor={this.props.isEditor} scaleCanvas={this.state.zoom} onLoadImage={this.onLoadImageObject} onAction={obj => this.triggerAction(obj)}/>
						</g>
					</g>
				);
			});
		}
		let componentsList = getComponentsList(this.objectsManager.objects);

		return (
			<div className="editor-app" 
				draggable={this.state.transferringObject && this.state.transferringObject.source && !this.state.transferringObject.component.meta.isPreventDrag} 
				onDragStart={ev => this.dragStartOuter(ev)}
				onKeyDown={this.handleKeyPress}
				onKeyPress={this.handleKeyPress}
				tabIndex="100"
				onDragEnd={this.onDragEnd}>
				{this.props.isEditor && (
					<div className="editor-app__controls controls">
						<Controls
							onCutClick={ e => { this.onChange(); this.bufferManager.cut() } }
							onCopyClick={ e => this.bufferManager.copy() }
							onPasteClick={ e => { this.onChange(); this.bufferManager.paste() } }
							onCloneClick={ e => { this.onChange(); this.bufferManager.clone() } }
							onUndoClick={ e => { this.onChange(); this.memento.undo() } }
							onRedoClick={ e => { this.onChange(); this.memento.redo() } }
							isSelectObject={ this.isSelectObject() }
							canUndo={ this.memento.canUndo() }
							canRedo={ this.memento.canRedo() }
						/>
					</div>
				)}
				{this.props.isEditor && (
					<div className="editor-app__components">
						<ComponentList components={Objects} onCreate={this.createComponent} />
					</div>
				)}
				{/* <BenchMark width={this.state.canvas.width} height={this.state.canvas.height} createComponent={this.createComponent.bind(this)}/> */}
			 
				<div className={'editor-app__container ' + (this.props.isEditor ? 'editor-app__container_editor' : 'editor-app__container_viewer')} ref={this.container}>
					<svg className="editor-app__canvas" height="100%"
						ref={this.svgCanvas}
						onMouseMove={this.drag}
						onMouseDown={e => { this.onCanvasClick(e); this.scrollDrag(e); }}
						onWheel={this.onMouseWheel}
						onClick={this.onClickObservArea}
						onDrop={this.onDrop} onDragOver={this.onDragOver}
						data-iteration={Math.random()}
						version="1.1" xmlns="http://www.w3.org/2000/svg">
						<SvgIcons id={this.id} canvasColor={this.state.canvas.backgroundColor} canvasGridColor={this.state.canvas.backgroundGridColor} />
						<g className="editor-app__viewport" transform={`translate(${this.state.scrollX} ${this.state.scrollY}) scale(${this.state.zoom}) `}>
							<PlanBackground
								image={this.state.canvas.backgroundImage}
								onLoadImage={e => {} }
								backgroundAlign={this.state.canvas.backgroundAlign}
								backgroundSize={this.state.canvas.backgroundSize}
								id={this.id}
								filter={this.state.canvas.filter}
								width={this.state.canvas.width}
								isEditor={this.props.isEditor}
								height={this.state.canvas.height} />

							{ componentsList }
							{this.props.isEditor && this.state.selectedComponentIndex >= 0 &&
								<Transformator onChangeEnd={this.onPropertyChangeEnd} onDeleteObject={this.deleteObject} zoom={this.state.zoom} onToggleLock={index => this.objectsManager.toggleObjectLock(index)} onMouseDown={this.startDrag} objectIndex={this.state.selectedComponentIndex} objectIteration={this.state.objectsIterations[this.state.selectedComponentIndex]} object={this.state.transformatorObject} />}
							{ currentEditor }
						</g>
					</svg>
					{this.props.isEditor && (
						<div className="editor-app__proporties">
							<PropertiesEditor
								moveZObjectUp={this.objectsManager.moveZObjectUp}
								moveZObjectDown={this.objectsManager.moveZObjectDown}
								object={this.state.transformatorObject}
								objectIndex={this.state.selectedComponentIndex}
								objectIteration={this.state.selectedComponentIndex >= 0 ? this.state.objectsIterations[this.state.selectedComponentIndex] : this.state.canvasIteration}
								onChange={this.onPropertyChange}
								onChangeEnd={this.onPropertyChangeEnd}
								isLastObject={this.isLastObject()}
								isFirstObject={this.isFirstObject()}
								parcels={this.propsParcel}
								createComponent={this.createComponent}
								selectObject={this.selectObject}
							/>
						</div>
					)}
					<ViewerPanel onZoom={this.addZoom} onZoomToSource={this.onZoomToSource} onZoomFill={this.onZoomFill} />
				</div>
			</div>
		);
	}
}



PlanEditor.propTypes = {
	// functions
	getSources: PropTypes.func,
	getMacroses: PropTypes.func,
	getWebPages: PropTypes.func,
	getTours: PropTypes.func,
	getPlans: PropTypes.func,
	getSalvos: PropTypes.func,
	// callbacks
	onDestroy: PropTypes.func,
	onDragStart: PropTypes.func,
	onDragEnd: PropTypes.func,
	onAction: PropTypes.func,
	onClickObservArea: PropTypes.func,
	onChange: PropTypes.func,
	// props
	isEditing: PropTypes.bool,
	isEditor: PropTypes.bool
}

export default PlanEditor;