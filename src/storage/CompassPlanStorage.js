/*global promisePyControllerCall, compass */
import PlanItem from "./PlanItem";
import IPlanStorage from "./IPlanStorage";

export default class CompassPlanStorage extends IPlanStorage {
    /**
     * Фабричный метод, который создает объект класса PlanItem
     * @param {String} description имя файла
     * @param {String} content содержимое файла
     */
    create(description, content) {
        const newPlan = new PlanItem(window.NULL_UUID, description, content);

        return newPlan;
    }

    /**
     * Сохранение плана
     * @param {PlanItem} plan графический план
     */
    async save(plan) {
        if (plan instanceof PlanItem) {
            return promisePyControllerCall(
                "PlanManager",
                "savePlan",
                compass.currentSite.master.ip,
                {
                    description: plan.description,
                    shortcut: plan.shortcut,
                    siteUuid: compass.currentSite.uuid,
                    currentStationAddress: compass.engineAddress,
                    data: plan.data,
                    uuid: plan.uuid
                }
            );
        }
    }

    /**
     * Удаление графического плана
     * @param {String} uuid uuid графического плана
     */
    async remove(uuid) {
        return promisePyControllerCall(
            "PlanManager",
            "removePlan",
            compass.currentSite.master.ip,
            {
                siteUuid: compass.currentSite.uuid,
                currentStationAddress: compass.engineAddress,
                uuid: uuid
            }
        );
    }

    /**
     * Получаем план по его uuid
     * @param {String} uuid compass uuid
     */
    async getByUuid(uuid) {
        if (!uuid) {
            return null;
        }

        const plan = await promisePyControllerCall(
            "PlanManager",
            "singlePlan",
            compass.currentSite.master.ip,
            {
                siteUuid: window.compass.currentSite.uuid,
                uuid: uuid
            }
        );

        return new PlanItem(
            plan.uuid,
            plan.description,
            plan.shortcut,
            plan.data
        );
    }

    /**
     * Получение списка графических планов
     */
    async getList() {
        return promisePyControllerCall(
            "PlanManager",
            "allPlans",
            compass.currentSite.master.ip,
            {
                siteUuid: compass.currentSite.uuid,
                currentStationAddress: compass.engineAddress
            }
        ).then(plans => {
            return plans.map(
                plan =>
                    new PlanItem(
                        plan.uuid,
                        plan.description,
                        plan.shortcut,
                        plan.data
                    )
            );
        });
    }
}

/*
    TODO: когда перепишем все на ReactJS, у нас будет общий бандл webpack, и можно
    будет избавиться от глобальной копии этого класса
*/
window.CompassPlanStorage = CompassPlanStorage;
