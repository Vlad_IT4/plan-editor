export default class PlanItem {
    constructor(uuid, description, data) {
        this.uuid = uuid;
        this.description = description;
        this.data = data;
    }
}
