export default class ImageItem {
    constructor(id, name, content) {
        this.id = id;
        this.name = name;
        this.content = content;
    }

    /**
     * Сохранение изображения в ядро
     */
    async save() {
        localStorage.setItem(`image_${this.id}`, JSON.stringify({
            id: this.id,
            name: this.name,
            content: this.content
        }));
    }

    /**
     * Удаление изображения с ядра
     */
    remove() {
        localStorage.removeItem(`image_${this.id}`);
    }

    /**
     * Переопределяем, чтобы при сериализации объекта плана, вместо объекта картинки подставлялся ее uuid
     */
    toString() {
        return this.id;
    }
    toJSON() {
        return this.toString();
    }
}
