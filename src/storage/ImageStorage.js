import ImageItem from "./ImageItem";
import { resizeImage } from "../Helpers";

export default class ImageStorage {
    /**
     * Фабричный метод, который создает объект класса ImageItem
     * @param {String} name имя файла
     * @param {String} content содержимое файла
     */
    static async createImage(name, mimeType, content) {
        let resizedContent = await resizeImage(content, mimeType, 3840, 2160); // max size Ultra HD 4K
        const newImg = new ImageItem(Math.random().toString(16).slice(2), name, resizedContent);
        await newImg.save();
        return newImg;
    }

    /**
     * Получаем изображение по его id
     * @param {String} id compass id
     */
    static getById(id) {
        if (!id) {
            throw new Error("id is undefined");
        }
        const img = JSON.parse(localStorage.getItem(`image_${id}`));
        if (!img) {
            return null;
        }
        return new ImageItem(img.id, img.name, img.content);
    }

    static getImagesListIds() {
        return Object.keys(localStorage).filter(key => key.startsWith('image_')).map(key => key.slice(6));
    }
}
