export default class IPlanStorage {
    create(name, content) {}
    save(plan) {}
    remove(plan) {}
    async getByUuid(uuid) {}
    async getList() {}
}
