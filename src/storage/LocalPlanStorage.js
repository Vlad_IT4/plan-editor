/*global promisePyControllerCall, compass */
import PlanItem from "./PlanItem";
import IPlanStorage from "./IPlanStorage";

export default class LocalPlanStorage extends IPlanStorage {
    /**
     * Фабричный метод, который создает объект класса PlanItem
     * @param {String} description имя файла
     * @param {String} content содержимое файла
     */
    create(description, content) {
        const newPlan = new PlanItem(window.NULL_UUID, description, content);

        return newPlan;
    }

    /**
     * Сохранение плана
     * @param {PlanItem} plan графический план
     */
    async save(plan) {
        if (plan instanceof PlanItem) {
            const dataObj = {
                description: plan.description,
                data: plan.data,
                uuid: plan.uuid
            }
            localStorage.setItem(`plan_${plan.uuid}`, JSON.stringify(dataObj));
        }
    }

    /**
     * Удаление графического плана
     * @param {String} uuid uuid графического плана
     */
    async remove(uuid) {
        localStorage.removeItem(`plan_${uuid}`);
    }

    /**
     * Получаем план по его uuid
     * @param {String} uuid compass uuid
     */
    async getByUuid(uuid) {
        if (!uuid) {
            return null;
        }

        const json = localStorage.getItem(`plan_${uuid}`);
        if (!json) {
            return;
        }
        const plan = JSON.parse(plan);

        return new PlanItem(
            plan.uuid,
            plan.description,
            plan.data
        );
    }

    /**
     * Получение списка графических планов
     */
    getList() {
        const uuids = Object.keys(localStorage).filter(key => key.startsWith('plan_'));
        return plans.map(
            uuid => {
                let plan = localStorage.getItem(uuid);
                return new PlanItem(
                    uuid.slice(5),
                    plan.description,
                    plan.data
                );
            }
        );
    }
}

/*
    TODO: когда перепишем все на ReactJS, у нас будет общий бандл webpack, и можно
    будет избавиться от глобальной копии этого класса
*/
window.CompassPlanStorage = CompassPlanStorage;
