import Command from "./commands/Command.js";
import CommandAddOrRemove from "./commands/CommandAddOrRemove.js";
import CommandProps from "./commands/CommandProps.js";
import CommandZMove from "./commands/CommandZMove.js";
import { debounce } from "../Helpers";

/**
 * Реализация паттерна хранитель. Служит для фиксации изменений объектов, и хранит их в объектах производных "Command"
 */
export default class Memento {
    constructor() {
        this._commands = [];
        this._currentIndex = 0;
        this._currentPosition = 1;
        this._currentObject = null;
        this._isLive = true;
        this.takeSnapshotDebounce = debounce(this.takeSnapshot, 500);
        window.memento = this;
    }
    static ignoreProps = [
        "ref",
        "sourceObject",
        "childs",
        "parentObject",
        "component",
        "parent",
        "childs"
    ];

    /**
     * Является ли свойство игнорируемым для фиксации изменений объекта
     * @param {String} prop название проверяемого свойства
     */
    isIgnoreProp(prop) {
        return Memento.ignoreProps.includes(prop);
    }

    /**
     * Добавить команду в список команд.
     * @param {Command} command добавляемая команда
     */
    addCommand(command) {
        const commandsLength = this._commands.length;
        if (commandsLength > 0 && this._currentIndex !== commandsLength - 1) {
            this._commands.splice(this._currentIndex + this._currentPosition);
            this._currentObject = null;
        }

        this._commands.push(command);
        this._currentIndex = this._commands.length - 1;
        this._isLive = true;
    }

    /**
     * Фиксируем изменение объекта
     * @param {Object} newObject измененный объект
     * @param {Number} oldIndex старый индекс выделенного объекта
     * @param {Number} newIndex новый индекс выделенного объекта
     */
    takeSnapshot(newObject, oldIndex, newIndex) {
        if (!(newObject && typeof newObject === "object")) {
            return false;
        }
        if (!this._currentObject) {
            return false;
        }
        const currentObject = this._currentObject.getRedoObject();

        if (currentObject) {
            const command = new CommandProps(currentObject, newObject);
            command.setIndex(oldIndex, newIndex);
            if (!command.isEmpty()) {
                this.addCommand(command);
            }
        }
        this.rememberObject(newObject);
        return true;
    }

    /**
     * Запоминаем объект, но не сохраняем в истории. Полученный объект будет являться объектом настоящего времени (т.к. не прошлый объект)
     * @param {Object} object измененный объект
     */
    rememberObject(object) {
        if (!(object && typeof object === "object")) {
            return false;
        }
        const cloneObject = {};

        Object.keys(object).forEach(prop => {
            if (this.isIgnoreProp(prop)) {
                return;
            }
            cloneObject[prop] = object[prop];
        });
        if (this._currentObject === null) {
            this._currentObject = new CommandProps();
        }
        this._currentObject.setRedoObject(cloneObject);
    }

    /**
     * Фиксируем удаление объекта с холста
     * @param {Number} pos индекс объекта в списке объектов (сохраняется для того, чтобы объект можно было вернуть на свою позицию при восстановлении)
     * @param {Object} object удаляемый объект
     * @param {Number} oldIndex старый индекс выделенного объекта
     * @param {Number} newIndex новый индекс выделенного объекта
     */
    deleteObject(pos, object, oldIndex, newIndex) {
        const command = new CommandAddOrRemove(object, pos, true);
        command.setIndex(oldIndex, newIndex);
        this.addCommand(command);
    }

    /**
     * Фиксируем создание объекта
     * @param {Number} pos индекс объекта в списке объектов (сохраняется для того, чтобы объект можно было вернуть на свою позицию при восстановлении)
     * @param {Object} object созданный объект
     * @param {Number} oldIndex старый индекс выделенного объекта
     * @param {Number} newIndex новый индекс выделенного объекта
     */
    addObject(pos, object, oldIndex, newIndex) {
        const command = new CommandAddOrRemove(object, pos, false);

        command.setIndex(oldIndex, newIndex);
        this.addCommand(command);
    }

    /**
     * Фиксируем перемещение объекта по слоям
     * @param {Number} objectId id объекта
     * @param {Number} from откуда перемещен объект
     * @param {Number} to куда перемещен объект
     * @param {Number} oldIndex старый индекс выделенного объекта
     * @param {Number} newIndex новый индекс выделенного объекта
     */
    moveZObject(objectId, from, to) {
        const command = new CommandZMove(objectId, from, to);
        this.addCommand(command);
    }

    /**
     * Фиксируем выделение объекта на холсте
     * @param {Number} oldIndex старый индекс выделенного объекта
     * @param {Number} newIndex новый индекс выделенного объекта
     */
    selectObject(oldIndex, newIndex) {
        const command = new Command();
        command.setIndex(oldIndex, newIndex);
        this.addCommand(command);
    }

    /**
     * Выполнить команду
     * @param {Number} cmdIndex индекс команды из списка команд
     * @param {Boolean} isUndo отмена а не возврат?
     */
    execute(cmdIndex, isUndo) {
        let cmd;
        // Если история команд кончилась, используем объект последнего состояния
        if (cmdIndex >= this._commands.length && this._commands.length > 0) {
            if (isUndo) {
                this._currentIndex = this._commands.length - 1;
                return this.execute(this._currentIndex, isUndo);
            } else {
                cmd = this._currentObject;
                this._isLive = true;
            }
        } else {
            cmd = this._commands[cmdIndex];
        }
        // Выполняем команду в соответствии с типом
        if (cmd instanceof CommandProps) {
            this.updateObject(
                cmd[isUndo ? "getUndoObject" : "getRedoObject"]()
            );
        } else if (cmd instanceof CommandAddOrRemove) {
            if ((isUndo && cmd.isRemove) || !(isUndo || cmd.isRemove)) {
                this.reviveObject(cmd.getDeadObject(), cmd.getPosition());
            } else if ((isUndo && !cmd.isRemove) || (!isUndo && cmd.isRemove)) {
                this.deadObject(cmd.getDeadObject(), cmd.getPosition());
            }
        } else if (cmd instanceof CommandZMove) {
            this.transferZObject(
                cmd.getObjectId(),
                ...cmd.getFromAndTo(isUndo)
            );
        }
        this.changeSelectIndex(cmd.getIndex(isUndo));
    }

    /**
     * Можно ли выполнить отмену?
     */
    canUndo() {
        return this._currentIndex >= 0 && this._commands.length > 0;
    }

    /**
     * Выполнение команды отмены
     */
    undo() {
        if (!this.canUndo()) {
            return;
        }
        this._isLive = false;
        this._currentIndex = Math.min(
            this._commands.length - 1,
            this._currentIndex
        );
        this.execute(this._currentIndex, true);
        this._currentIndex--;
        this._currentPosition = 1;
    }

    /**
     * Можно ли выполнить возврат?
     */
    canRedo() {
        return this._currentIndex < this._commands.length && !this._isLive;
    }

    /**
     * Выполняем команду возврата
     */
    redo() {
        this._currentIndex = Math.max(0, this._currentIndex);
        this.execute(this._currentIndex, false);
        this._currentIndex++;
        this._currentPosition = 0;
    }
}
