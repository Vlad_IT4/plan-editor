import Command from "./Command.js";

/**
 * Паттерн команда. Делает снимок удалений и добавлений объектов
 */
export default class CommandAddOrRemove extends Command {
    /**
     * @param {Object} object объект на холсте
     * @param {Number} pos индекс объекта в списке объектов
     * @param {Boolean} isRemove true - удаление, false создание
     */
    constructor(object, pos, isRemove = true) {
        super();
        this._object = object;
        this._pos = pos;
        this.isRemove = isRemove;
    }

    /**
     * Получить удаленный объект
     */
    getDeadObject() {
        return this._object;
    }

    /**
     * Получить индекс объекта в списке объекто
     */
    getPosition() {
        return this._pos;
    }
}
