/**
 * Реализация паттерна Команда. Служит для создания снимка изменения объекта
 */
export default class Command {
    constructor() {
        this._oldIndex = -1;
        this._newIndex = -1;
    }
    /**
     * Получаем предыдущий индекс выбранного объекта на холсте
     */
    getOldIndex() {
        // Если нет oldIndex, используем newIndex
        return this._oldIndex === null ? this._newIndex : this._oldIndex;
    }

    /**
     * Получаем индекс выбранного объекта на холсте
     */
    getNewIndex() {
        // Если нет newIndex, используем oldIndex
        return this._newIndex === null ? this._oldIndex : this._newIndex;
    }

    /**
     * Получаем индекс выбранного объекта. Для отмены предыдущий, для возврата текущий
     * @param {Boolean} isUndo отмена?
     */
    getIndex(isUndo) {
        return isUndo ? this.getOldIndex() : this.getNewIndex();
    }

    /**
     * Устанавливаем индекс выбранного объекта
     * @param {Number} oldIndex предыдущий индекс
     * @param {Number} newIndex текущий индекс
     */
    setIndex(oldIndex, newIndex) {
        this._oldIndex = oldIndex;
        this._newIndex = newIndex;
    }
}
