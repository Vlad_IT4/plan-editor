import Command from "./Command.js";

/**
 * Паттерн команда. Делает снимок измененных свойств объекта
 */
export default class CommandProps extends Command {
    /**
     * @param {Object} oldObject копия объекта до его изменения
     * @param {Object} newObject копия объекта после изменения
     */
    constructor(oldObject = {}, newObject = {}) {
        super();
        const { oldDiffObject, newDiffObject } = this.getDiff(
            oldObject,
            newObject
        );
        this._undoObject = oldDiffObject || {};
        this._redoObject = newDiffObject || {};
    }

    /**
     * Есть ли разница между старым и новым объектами
     */
    isEmpty() {
        return Object.keys(this._undoObject).length <= 1; // empty or only with id
    }

    /**
     * Является ли свойство игнорируемым для фиксации изменений объекта
     * @param {String} prop название проверяемого свойства
     */
    isIgnoreProp(prop) {
        return [
            "ref",
            "sourceObject",
            "childs",
            "parentObject",
            "component",
            "parent",
            "childs"
        ].includes(prop);
    }

    /**
     * Получаем список всех свойств староного и нового объектов
     * @param {Number} objectOld копия объекта до его изменения
     * @param {*} objectNew копия объекта после изменения
     */
    getCommonFields(objectOld, objectNew) {
        return [
            ...new Set(Object.keys(objectOld).concat(Object.keys(objectNew)))
        ].filter(prop => !this.isIgnoreProp(prop));
    }

    /**
     * Получаем разницу между объектами
     * @param {Object} oldObject копия объекта до его изменения
     * @param {*} newObject копия объекта после изменения
     */
    getDiff(oldObject, newObject) {
        const oldDiffObject = {};
        const newDiffObject = {};
        this.getCommonFields(oldObject, newObject).forEach(field => {
            const valOld = oldObject[field];
            const valNew = newObject[field];

            if (valOld !== valNew) {
                oldDiffObject[field] = valOld;
                newDiffObject[field] = valNew;
            }
        });
        oldDiffObject.id = oldObject.id;
        newDiffObject.id = newObject.id;

        return { oldDiffObject, newDiffObject };
    }

    /**
     * Получаем объект для применения отмены
     */
    getUndoObject() {
        return this._undoObject;
    }

    /**
     * Получаем объект для применения возврата
     */
    getRedoObject() {
        return this._redoObject;
    }

    /**
     * Установкао объекта для возврата
     * @param {Object} redoObject объект для возврата
     */
    setRedoObject(redoObject) {
        this._redoObject = redoObject;
    }
}
