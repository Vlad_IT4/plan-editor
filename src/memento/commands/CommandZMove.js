import Command from "./Command.js";

/**
 * Паттерн команда. Делает снимок перемещения объекта по слоям
 */
export default class CommandZMove extends Command {
    /**
     * @param {Number} objectId id объекта
     * @param {Number} from предыдущий индекс объекта в списке объектов
     * @param {Number} to новый индекс объекта в писке объектов
     */
    constructor(objectId, from, to) {
        super();
        this._from = from;
        this._to = to;
        this._objectId = objectId;
    }

    /**
     * Получить массив вида [from, to] для применения перемещения объекта в списке объектов с позиции from на позицию to
     * @param {Boolean} isUndo true - отмена, false - возврат
     */
    getFromAndTo(isUndo) {
        return isUndo ? [this._to, this._from] : [this._from, this._to];
    }

    /**
     * Получить id перемещаемого объекта
     */
    getObjectId() {
        return this._objectId;
    }
}
