export default class ObjectsTransformator {
    constructor(plan) {
        this._plan = plan;
    }

    /**
     * Поворот объекта мышкой
     */
    objectRotate(object, mousePos) {
        const objectScale = object.scale || 1;
        const objectBBox = object.ref.current.getBBox();

        var x = object.x + (objectBBox.width / 2) * objectScale;
        var y = object.y + (objectBBox.height / 2) * objectScale;
        let mouseX =
            mousePos.x - this._plan.state.scrollX / this._plan.state.zoom;
        let mouseY =
            mousePos.y - this._plan.state.scrollY / this._plan.state.zoom;

        var radian = Math.atan2(mouseX - x, mouseY - y);
        var rotate = radian * (180 / Math.PI) * -1 + 90;
        object.angle = rotate;
    }

    /**
     * Масштабирование объекта мышкой
     */
    objectScale(object, mousePos) {
        const diffMouseX = mousePos.x - this._plan.state.startParams.mousePos.x;
        const diffMouseY = mousePos.y - this._plan.state.startParams.mousePos.y;
        const newScale =
            this._plan.state.startParams.object.scale +
            (diffMouseX + diffMouseY) * 0.005;

        object.scale = Math.max(0.2, newScale);
    }

    /**
     * Перемещение точки объекта за мышкой
     */
    objectVectorMove(object, mousePos, mode) {
        const newX =
            mousePos.x -
            object.x -
            this._plan.state.scrollX / this._plan.state.zoom;
        const newY =
            mousePos.y -
            object.y -
            this._plan.state.scrollY / this._plan.state.zoom;
        if (mode === "vector_end") {
            object.x2 = newX;
            object.y2 = newY;
        } else if (mode === "vector_start") {
            object.x1 = newX;
            object.y1 = newY;
        }
        const minX = Math.min(...[object.x1, object.x2]);
        object.x1 -= minX;
        object.x2 -= minX;
        object.x += minX;

        const minY = Math.min(...[object.y1, object.y2]);
        object.y1 -= minY;
        object.y2 -= minY;
        object.y += minY;
    }

    /**
     * Перемещение объекта за мышкой
     */
    objectMove(object, mousePos) {
        object.x = Math.round(
            mousePos.x + this._plan.state.startParams.offset.x
        );
        object.y = Math.round(
            mousePos.y + this._plan.state.startParams.offset.y
        );
    }

    /**
     * Изменение размера объекта по позиции мыши
     */
    objectResize(object, mousePos, mode, isProportional) {
        const diffMouseX = mousePos.x - this._plan.state.startParams.mousePos.x;
        const diffMouseY = mousePos.y - this._plan.state.startParams.mousePos.y;
        let resizeModes = mode.substr(7).split("");
        let changeWidth = false,
            changeHeight = false;
        const minWidth = object.component.meta.properties.width.minValue;
        const minHeight = object.component.meta.properties.height.minValue;

        // Change size
        if (resizeModes.includes("t")) {
            object.height = Math.max(
                minHeight,
                -diffMouseY + this._plan.state.startParams.object.height
            );
            changeHeight = true;
        } else if (resizeModes.includes("b")) {
            object.height = Math.max(
                minHeight,
                diffMouseY + this._plan.state.startParams.object.height
            );
            changeHeight = true;
        }

        if (resizeModes.includes("l")) {
            object.width = Math.max(
                minWidth,
                -diffMouseX + this._plan.state.startParams.object.width
            );
            changeWidth = true;
        } else if (resizeModes.includes("r")) {
            object.width = Math.max(
                minWidth,
                diffMouseX + this._plan.state.startParams.object.width
            );
            changeWidth = true;
        }

        if (isProportional) {
            if (changeHeight) {
                const rh =
                    this._plan.state.startParams.object.height / object.height;
                object.width = this._plan.state.startParams.object.width / rh;
            } else if (changeWidth) {
                const rw =
                    this._plan.state.startParams.object.width / object.width;
                object.height = this._plan.state.startParams.object.height / rw;
            }
        }

        // Change positions
        if (resizeModes.includes("t")) {
            const diffHeight =
                this._plan.state.startParams.object.height - object.height;
            object.y = Math.min(
                this._plan.state.startParams.object.y +
                    this._plan.state.startParams.object.height,
                this._plan.state.startParams.object.y + diffHeight
            );
        }

        if (resizeModes.includes("l")) {
            const diffWidth =
                this._plan.state.startParams.object.width - object.width;
            object.x = Math.min(
                this._plan.state.startParams.object.x +
                    this._plan.state.startParams.object.width,
                this._plan.state.startParams.object.x + diffWidth
            );
        }
    }
}
