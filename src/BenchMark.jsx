import React from "react";
import PropTypes from "prop-types";
import Objects from "./components/objects/objects.js";

/**
 * Класс для бенчмарк тестирования, для проверки, сколько объектов одновременно можно нарисовать
 */
export default class BenchMark extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            current: 0,
            limit: 100,
            benching: false
        };
        this.timer = null;
    }
    shouldComponentUpdate(nextProps, nextState) {
        return (
            nextState.current !== this.state.current ||
            nextState.limit !== this.state.limit ||
            nextState.benching !== this.state.benching
        );
    }
    stopBench = () => {
        clearInterval(this.timer);
        this.setState({
            benching: false
        });
    };
    startBench = () => {
        this.setState({ current: 0, benching: true });
        this.timer = setInterval(() => {
            let comp = Object.values(Objects)[
                Math.floor((Object.values(Objects).length - 1) * Math.random())
            ];
            if (
                comp === Objects["CanvasPlan"] ||
                comp === Objects["PathComponent"]
            ) {
                return;
            }
            const x = ~~(this.props.width * Math.random());
            const y = ~~(this.props.height * Math.random());
            this.props.createComponent(comp, x, y);
            this.setState({ current: this.state.current + 1 });
            if (this.state.current >= this.state.limit) {
                this.stopBench();
            }
        }, 40);
    };

    render() {
        return (
            <div className="benchmark">
                <h4 className="benchmark__title">
                    Количество: {this.state.current}
                </h4>
                <input
                    className="benchmark__input"
                    type="text"
                    disabled={this.state.benching}
                    onChange={e => this.setState({ limit: +e.target.value })}
                    defaultValue={this.state.limit}
                />
                {this.state.benching ? (
                    <button
                        className="benchmark__button"
                        onClick={this.stopBench}
                    >
                        Stop
                    </button>
                ) : (
                    <button
                        className="benchmark__button"
                        onClick={this.startBench}
                    >
                        Start
                    </button>
                )}
            </div>
        );
    }
}

BenchMark.PropTypes = {
    createComponent: PropTypes.func,
    width: PropTypes.number,
    height: PropTypes.number
};
