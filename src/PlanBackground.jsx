import React from "react";
import PropTypes from "prop-types";

/**
 * Фон графического плана
 */
export default class PlanBackground extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {};
    }

	/**
	 * Устанавливаем размеры фона, как только появится картинка
	 */
    setBackgroundSize(ref) {
        if (!ref) {
            return;
        }
        const bbox = ref.getBBox();
        this.setState({
            imageWidth: bbox.width,
            imageHeight: bbox.height
        });
    }

    onLoad = e => {
        this.props.onLoadImage();
    };

    render() {
        let preserveAspectRatio, backWidth, backHeight;
        backWidth = this.props.width;
        backHeight = this.props.height;

        let left = 0;
        let top = 0;

        if (this.props.image) {
            if (!this.props.backgroundSize) {
                preserveAspectRatio = `none`;
            } else if (this.props.backgroundSize === "original") {
                preserveAspectRatio = "none";
                backWidth = backHeight = null;
                const backgroundAlign = this.props.backgroundAlign;
                if (backgroundAlign && this.state.imageWidth !== undefined) {
                    if (backgroundAlign.includes("xMax")) {
                        left = -this.state.imageWidth + this.props.width;
                    } else if (backgroundAlign.includes("xMid")) {
                        left = this.props.width / 2 - this.state.imageWidth / 2;
                    }

                    if (backgroundAlign.includes("YMax")) {
                        top = -this.state.imageHeight + this.props.height;
                    } else if (backgroundAlign.includes("YMid")) {
                        top =
                            this.props.height / 2 - this.state.imageHeight / 2;
                    }
                }
            } else {
                preserveAspectRatio = `${this.props.backgroundAlign} ${
                    this.props.backgroundSize
                }`;
            }
        }
        let backPattern = `url(#bg-${this.props.id})`;
        let backColor = "#1c1e22";
        let backFill = backColor;

        if (this.props.isEditor) {
            backFill = `${backPattern} ${backColor}`;
        }
        return (
            <g>
                <rect
                    style={{ fill: backFill }}
                    x="0"
                    y="0"
                    height={`${this.props.height}`}
                    width={`${this.props.width}`}
                />
                <defs>
                    <clipPath id={`block-overflow-${this.props.id}`}>
                        <rect
                            height={`${this.props.height}`}
                            width={`${this.props.width}`}
                            fill="none"
                            stroke="blue"
                        />
                    </clipPath>
                </defs>
                {this.props.image && (
                    <g>
                        <filter id={`filter-${this.props.id}`}>
                            <feColorMatrix
                                type="matrix"
                                values={this.props.filter}
                            />
                        </filter>
                        <g
                            filter={`url(#filter-${this.props.id})`}
                            clipPath={`url(#block-overflow-${this.props.id})`}
                        >
                            <image
                                onLoad={this.onLoad}
                                xlinkHref={this.props.image.content}
                                transform={`translate(${left} ${top})`}
                                preserveAspectRatio={preserveAspectRatio}
                                height={backHeight}
                                width={backWidth}
                                ref={ref => this.setBackgroundSize(ref)}
                            />
                        </g>
                    </g>
                )}
            </g>
        );
    }
}

PlanBackground.propTypes = {
    onLoadImage: PropTypes.func,
    width: PropTypes.number,
    height: PropTypes.number,
    id: PropTypes.string,
    backgroundSize: PropTypes.string,
    backgroundAlign: PropTypes.string,
    isEditor: PropTypes.bool,
    filter: PropTypes.string
};
